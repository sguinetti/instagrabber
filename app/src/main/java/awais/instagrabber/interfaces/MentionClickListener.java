package awais.instagrabber.interfaces;

public interface MentionClickListener {
    void onClick(final String text, final boolean isHashtag);
}