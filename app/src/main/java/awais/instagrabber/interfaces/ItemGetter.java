package awais.instagrabber.interfaces;

import java.io.Serializable;
import java.util.List;

import awais.instagrabber.models.BasePostModel;

public interface ItemGetter extends Serializable {
    List<? extends BasePostModel> get(final ItemGetType itemGetType);
    enum ItemGetType implements Serializable {
        MAIN_ITEMS, DISCOVER_ITEMS, FEED_ITEMS,
    }
}