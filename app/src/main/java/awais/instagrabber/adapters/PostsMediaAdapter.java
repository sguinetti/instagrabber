package awais.instagrabber.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import awais.instagrabber.R;
import awais.instagrabber.models.BasePostModel;
import awais.instagrabber.models.ViewerPostModel;

public class PostsMediaAdapter extends RecyclerView.Adapter<PostsMediaAdapter.PostMediaViewHolder> {
    private final View.OnClickListener clickListener;
    private LayoutInflater layoutInflater;
    private BasePostModel[] postModels;

    public PostsMediaAdapter(final BasePostModel[] postModels, final View.OnClickListener clickListener) {
        this.postModels = postModels;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public PostMediaViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        if (layoutInflater == null) layoutInflater = LayoutInflater.from(parent.getContext());
        return new PostMediaViewHolder(layoutInflater.inflate(R.layout.item_child_post, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final PostMediaViewHolder holder, final int position) {
        final BasePostModel postModel = postModels[position];
        if (postModel != null) {
            postModel.setPosition(position);

            holder.itemView.setTag(postModel);
            holder.itemView.setOnClickListener(clickListener);

            holder.isDownloaded.setVisibility(postModel.isDownloaded() ? View.VISIBLE : View.GONE);

            final String sliderDisplayUrl = postModel instanceof ViewerPostModel ? ((ViewerPostModel) postModel).getSliderDisplayUrl()
                    : postModel.getDisplayUrl();

            Picasso.get().load(sliderDisplayUrl).into(holder.icon);
        }
    }

    public void setData(final BasePostModel[] postModels) {
        this.postModels = postModels;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return postModels == null ? 0 : postModels.length;
    }

    public BasePostModel[] getPostModels() {
        return postModels;
    }

    public final static class PostMediaViewHolder extends RecyclerView.ViewHolder {
        public final ImageView icon, isDownloaded;

        public PostMediaViewHolder(@NonNull final View itemView) {
            super(itemView);
            isDownloaded = itemView.findViewById(R.id.isDownloaded);
            icon = itemView.findViewById(R.id.icon);
        }
    }
}