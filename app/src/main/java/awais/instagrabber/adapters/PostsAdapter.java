package awais.instagrabber.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import awais.instagrabber.R;
import awais.instagrabber.adapters.viewholder.PostViewHolder;
import awais.instagrabber.models.PostModel;

public final class PostsAdapter extends RecyclerView.Adapter<PostViewHolder> {
    private final ArrayList<PostModel> postModels;
    private final View.OnClickListener clickListener;
    private final View.OnLongClickListener longClickListener;
    private LayoutInflater layoutInflater;
    public boolean isSelecting = false;

    public PostsAdapter(final ArrayList<PostModel> postModels, final View.OnClickListener clickListener,
                        final View.OnLongClickListener longClickListener) {
        this.postModels = postModels;
        this.clickListener = clickListener;
        this.longClickListener = longClickListener;
    }

    @NonNull
    @Override
    public PostViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        if (layoutInflater == null) layoutInflater = LayoutInflater.from(parent.getContext());
        return new PostViewHolder(layoutInflater.inflate(R.layout.item_post, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final PostViewHolder holder, final int position) {
        final PostModel postModel = postModels.get(position);
        if (postModel != null) {
            postModel.setPosition(position);

            holder.itemView.setTag(postModel);

            holder.itemView.setOnClickListener(clickListener);
            holder.itemView.setOnLongClickListener(longClickListener);

            final boolean slider = postModel.isSlider();
            final String thumbnailUrl = postModel.getThumbnailUrl();

            holder.isDownloaded.setVisibility(postModel.isDownloaded() ? View.VISIBLE : View.GONE);

            holder.typeIcon.setVisibility(postModel.isVideo() || slider ? View.VISIBLE : View.GONE);
            holder.typeIcon.setImageResource(slider ? R.drawable.slider : R.drawable.video);

            holder.selectedView.setVisibility(postModel.isSelected() ? View.VISIBLE : View.GONE);
            holder.progressView.setVisibility(View.VISIBLE);

            Picasso.get().load(thumbnailUrl).into(holder.postImage, new Callback() {
                @Override
                public void onSuccess() {
                    holder.progressView.setVisibility(View.GONE);
                }

                @Override
                public void onError(final Exception e) {
                    holder.progressView.setVisibility(View.GONE);
                    Picasso.get().load(postModel.getDisplayUrl()).into(holder.postImage);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return postModels == null ? 0 : postModels.size();
    }
}
