package awais.instagrabber.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import awais.instagrabber.R;
import awais.instagrabber.models.StoryModel;

public class StoriesAdapter extends RecyclerView.Adapter<StoriesAdapter.StoryViewHolder> {
    private final View.OnClickListener clickListener;
    private LayoutInflater layoutInflater;
    private StoryModel[] storyModels;

    public StoriesAdapter(final StoryModel[] storyModels, final View.OnClickListener clickListener) {
        this.storyModels = storyModels;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public StoryViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        if (layoutInflater == null) layoutInflater = LayoutInflater.from(parent.getContext());
        return new StoryViewHolder(layoutInflater.inflate(R.layout.item_story, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final StoryViewHolder holder, final int position) {
        final StoryModel storyModel = storyModels[position];
        if (storyModel != null) {
            storyModel.setPosition(position);
            holder.itemView.setTag(storyModel);
            holder.itemView.setOnClickListener(clickListener);
            Picasso.get().load(storyModel.getStoryUrl()).into(holder.icon);
        }
    }

    public void setData(final StoryModel[] storyModels) {
        this.storyModels = storyModels;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return storyModels == null ? 0 : storyModels.length;
    }

    public final static class StoryViewHolder extends RecyclerView.ViewHolder {
        public final ImageView icon;

        public StoryViewHolder(@NonNull final View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.icon);
        }
    }
}