package awais.instagrabber.asyncs;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;

import awais.instagrabber.BuildConfig;
import awais.instagrabber.interfaces.FetchListener;
import awais.instagrabber.models.StoryModel;
import awais.instagrabber.utils.Constants;
import awais.instagrabber.utils.Utils;
import awaisomereport.LogCollector;

import static awais.instagrabber.utils.Utils.logCollector;

public final class StoryStatusFetcher extends AsyncTask<Void, Void, StoryModel[]> {
    private final String id;
    private final FetchListener<StoryModel[]> fetchListener;

    public StoryStatusFetcher(final String id, final FetchListener<StoryModel[]> fetchListener) {
        this.id = id;
        this.fetchListener = fetchListener;
    }

    @Override
    protected StoryModel[] doInBackground(final Void... voids) {
        StoryModel[] result = null;
        final String url = "https://www.instagram.com/graphql/query/?query_hash=52a36e788a02a3c612742ed5146f1676&variables=%7B%22precomposed_overlay%22%3Afalse%2C%22reel_ids%22%3A%5B%22"
                + id + "%22%5D%7D";

        try {
            final HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
            conn.setInstanceFollowRedirects(false);
            conn.setUseCaches(false);
            conn.connect();

            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                JSONObject data = new JSONObject(Utils.readFromConnection(conn)).getJSONObject("data");
                if (data.has("reels_media")) {
                    JSONArray media = data.getJSONArray("reels_media");
                    if (media.length() > 0) {
                        data = media.getJSONObject(0);
                        if (data.has("items")) {
                            media = data.getJSONArray("items");
                            final int len = media.length();
                            final StoryModel[] models = new StoryModel[len];
                            for (int i = 0; i < len; ++i) {
                                data = media.getJSONObject(i);
                                final boolean isVideo = data.getBoolean("is_video");

                                boolean hasTappableObjecs = data.has("tappable_objects");
                                final JSONArray tappableObjects;
                                final int tappableLength;
                                if (hasTappableObjecs) {
                                    tappableObjects = data.getJSONArray("tappable_objects");
                                    tappableLength = tappableObjects.length();
                                    hasTappableObjecs = tappableLength > 0;
                                } else {
                                    tappableLength = 0;
                                    tappableObjects = null;
                                }

                                models[i] = new StoryModel(data.getString(Constants.EXTRAS_ID), data.getString("display_url"),
                                        isVideo, data.getLong("taken_at_timestamp"));

                                if (isVideo && data.has("video_resources"))
                                    models[i].setVideoUrl(Utils.getHighQualityPost(data.getJSONArray("video_resources"), true));

                                if (hasTappableObjecs) {
                                    for (int j = 0; j < tappableLength; ++j) {
                                        JSONObject jsonObject = tappableObjects.getJSONObject(j);
                                        if (jsonObject.getString("__typename").equals("GraphTappableFeedMedia")) {
                                            jsonObject = jsonObject.getJSONObject("media");
                                            models[i].setTappableShortCode(jsonObject.getString(Constants.EXTRAS_SHORTCODE));
                                            break;
                                        }
                                    }
                                }
                            }
                            result = models;
                        }
                    }
                }
            }

            conn.disconnect();
        } catch (final Exception e) {
            if (logCollector != null)
                logCollector.appendException(e, LogCollector.LogFile.ASYNC_STORY_STATUS_FETCHER, "doInBackground");
            if (BuildConfig.DEBUG) Log.e("AWAISKING_APP", "", e);
        }

        return result;
    }

    @Override
    protected void onPreExecute() {
        if (fetchListener != null) fetchListener.doBefore();
    }

    @Override
    protected void onPostExecute(final StoryModel[] result) {
        if (fetchListener != null) fetchListener.onResult(result);
    }
}