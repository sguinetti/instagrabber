package awais.instagrabber.asyncs;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InterruptedIOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import awais.instagrabber.interfaces.FetchListener;
import awais.instagrabber.models.SuggestionModel;
import awais.instagrabber.utils.Constants;
import awais.instagrabber.utils.Utils;

public class SuggestionsFetcher extends AsyncTask<String, String, SuggestionModel[]> {
    private final FetchListener<SuggestionModel[]> fetchListener;

    public SuggestionsFetcher(final FetchListener<SuggestionModel[]> fetchListener) {
        this.fetchListener = fetchListener;
    }

    @Override
    protected void onPreExecute() {
        if (fetchListener != null) fetchListener.doBefore();
    }

    @Override
    protected SuggestionModel[] doInBackground(final String... params) {
        SuggestionModel[] result = null;
        try {
            final HttpURLConnection conn = (HttpURLConnection) new URL("https://www.instagram.com/web/search/topsearch/?context=blended&count=50&query="
                    + params[0]).openConnection();
            conn.setUseCaches(false);
            conn.connect();

            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                final JSONObject jsonObject = new JSONObject(Utils.readFromConnection(conn));
                conn.disconnect();

                final JSONArray usersArray = jsonObject.getJSONArray("users");
                final JSONArray hashtagsArray = jsonObject.getJSONArray("hashtags");

                final int usersLen = usersArray.length();
                final int hashtagsLen = hashtagsArray.length();

                final ArrayList<SuggestionModel> suggestionModels = new ArrayList<>(usersLen + hashtagsLen);
                for (int i = 0; i < hashtagsLen; i++) {
                    final JSONObject hashtag = hashtagsArray.getJSONObject(i).getJSONObject("hashtag");
                    suggestionModels.add(new SuggestionModel(false, hashtag.getString(Constants.EXTRAS_NAME), null,
                            hashtag.getString("profile_pic_url"), SuggestionType.TYPE_HASHTAG));
                }

                for (int i = 0; i < usersLen; i++) {
                    final JSONObject user = usersArray.getJSONObject(i).getJSONObject(Constants.EXTRAS_USER);
                    suggestionModels.add(new SuggestionModel( user.getBoolean("is_verified"),
                            user.getString(Constants.EXTRAS_USERNAME), user.getString("full_name"),
                            user.getString("profile_pic_url"), SuggestionType.TYPE_USER));
                }

                suggestionModels.trimToSize();

                result = suggestionModels.toArray(new SuggestionModel[0]);
            }

            conn.disconnect();
        } catch (final Exception e) {
            if (!(e instanceof InterruptedIOException)) Log.e("AWAISKING_APP", "", e);
        }
        return result;
    }

    @Override
    protected void onPostExecute(final SuggestionModel[] result) {
        if (fetchListener != null) fetchListener.onResult(result);
    }

    public enum SuggestionType {TYPE_USER, TYPE_HASHTAG,}
}