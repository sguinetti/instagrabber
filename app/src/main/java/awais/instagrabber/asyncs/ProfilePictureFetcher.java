package awais.instagrabber.asyncs;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.HttpURLConnection;
import java.net.URL;

import awais.instagrabber.BuildConfig;
import awais.instagrabber.interfaces.FetchListener;
import awais.instagrabber.utils.Utils;

public class ProfilePictureFetcher extends AsyncTask<Void, Void, String> {
    private final FetchListener<String> fetchListener;
    private final String userName, userId;
    private final boolean alternativeMethod;

    public ProfilePictureFetcher(final String userName, final String userId, final FetchListener<String> fetchListener, final boolean altMethod) {
        this.fetchListener = fetchListener;
        this.userName = userName;
        this.userId = userId;
        this.alternativeMethod = altMethod;
    }

    @Override
    protected String doInBackground(final Void... voids) {
        String out = null;
        try {
            final boolean isAlternativeMethod = alternativeMethod && !Utils.isEmpty(userId);

            final String url;
            // todo add https://www.instadp.com/fullsize/
            if (!isAlternativeMethod) url = "https://insta-stalkerr.com/instadp_fullsize/?id=" + userName;
            else {
                url = "https://instafullsize.com/ifsapi/ig/photo/s1/" + userName + "?igid=" + userId; // select from s1, s2, s3 but s1 works fine
            }

            // prolly http://167.99.85.4/instagram/userid?profile-url=the.badak

            final HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
            conn.setUseCaches(false);

            if (isAlternativeMethod) {
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Authorization", "fjgt842ff582a");
            }

            final String result = conn.getResponseCode() == HttpURLConnection.HTTP_OK ? Utils.readFromConnection(conn) : null;
            conn.disconnect();

            if (!Utils.isEmpty(result)) {
                final Document doc = Jsoup.parse(result);
                boolean fallback = false;

                if (alternativeMethod) {
                    try {
                        final JSONObject object = new JSONObject(result);
                        out = object.getString("result");
                    } catch (final Exception e) {
                        if (BuildConfig.DEBUG) Log.e("AWAISKING_APP", "", e);
                        fallback = true;
                    }
                } else {
                    final Elements elements = doc.select("img[data-src]");
                    if (elements.size() > 0) out = elements.get(0).attr("data-src");
                    else fallback = true;
                }

                if (fallback) {
                    final Elements imgs = doc.getElementsByTag("img");
                    for (final Element img : imgs) {
                        final String imgStr = img.toString();
                        if (imgStr.contains("cdninstagram.com")) return img.attr("src");
                    }
                }
            }
        } catch (Exception e) {
            if (BuildConfig.DEBUG) Log.e("AWAISKING_APP", "", e);
        }

        return out;
    }

    @Override
    protected void onPreExecute() {
        if (fetchListener != null) fetchListener.doBefore();
    }

    @Override
    protected void onPostExecute(final String result) {
        if (fetchListener != null) fetchListener.onResult(result);
    }
}
