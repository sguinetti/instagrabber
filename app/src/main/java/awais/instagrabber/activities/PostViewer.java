package awais.instagrabber.activities;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GestureDetectorCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MediaSourceEventListener;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import awais.instagrabber.R;
import awais.instagrabber.adapters.PostsMediaAdapter;
import awais.instagrabber.asyncs.PostFetcher;
import awais.instagrabber.asyncs.ProfileFetcher;
import awais.instagrabber.customviews.CommentMentionClickSpan;
import awais.instagrabber.customviews.RamboTextView;
import awais.instagrabber.interfaces.ItemGetter;
import awais.instagrabber.interfaces.SwipeEvent;
import awais.instagrabber.models.BasePostModel;
import awais.instagrabber.models.PostModel;
import awais.instagrabber.models.ProfileModel;
import awais.instagrabber.models.ViewerPostModel;
import awais.instagrabber.utils.Constants;
import awais.instagrabber.utils.SwipeListener;
import awais.instagrabber.utils.Utils;

public final class PostViewer extends AppCompatActivity {
    private String url, prevUsername;
    private ProfileModel profileModel;
    private BasePostModel postModel;
    private ViewerPostModel viewerPostModel;
    private SimpleExoPlayer player;
    private ArrayAdapter<String> profileDialogAdapter;
    private RecyclerView mediaList;
    private PlayerView playerView;
    private RamboTextView viewerCaption, title;
    private PhotoView photoView;
    private View progressView, controller, btnDownload, btnComments, viewsContainer, viewerCaptionParent;
    private GestureDetectorCompat gestureDetector;
    private SwipeEvent swipeEvent;
    private CharSequence postCaption = null, postShortCode;
    private ImageView btnMute, ivProfilePic;
    private TextView tvCommentsCount, tvVideoViews, tvPostDate;
    private Resources resources;
    private boolean session = false, isFromShare;
    private int slidePos = 0;
    private ItemGetter.ItemGetType itemGetType;
    private final DialogInterface.OnClickListener profileDialogListener = (dialog, which) -> {
        final String username = viewerPostModel.getUsername();

        if (which == 0) {
            searchUsername(username);
        } else if (profileModel != null && which == 1) {
            startActivity(new Intent(this, ProfileViewer.class)
                    .putExtra(Constants.EXTRAS_PROFILE, profileModel));
        }
    };
    private final View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            if (v == ivProfilePic) {
                new AlertDialog.Builder(PostViewer.this).setAdapter(profileDialogAdapter, profileDialogListener)
                        .setNeutralButton(R.string.cancel, null).setTitle(viewerPostModel.getUsername()).show();

            } else {
                final Object tag = v.getTag();
                if (tag instanceof ViewerPostModel) {
                    viewerPostModel = (ViewerPostModel) tag;
                    slidePos = Math.max(0, viewerPostModel.getPosition());
                    refreshPost();
                }
            }
        }
    };
    private final PostsMediaAdapter mediaAdapter = new PostsMediaAdapter(null, onClickListener);

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewer);

        final Intent intent = getIntent();
        if (intent == null || !intent.hasExtra(Constants.EXTRAS_POST)
                || (postModel = (PostModel) intent.getSerializableExtra(Constants.EXTRAS_POST)) == null) {
            Utils.errorFinish(this);
            return;
        }

        if (intent.hasExtra(Constants.EXTRAS_TYPE))
            itemGetType = (ItemGetter.ItemGetType) intent.getSerializableExtra(Constants.EXTRAS_TYPE);

        resources = getResources();

        final View viewStoryPost = findViewById(R.id.viewStoryPost);
        if (viewStoryPost != null) viewStoryPost.setVisibility(View.GONE);
        ivProfilePic = findViewById(R.id.ivProfilePic);
        title = findViewById(R.id.title);

        title.setMovementMethod(new LinkMovementMethod());
        title.setMentionClickListener((text, isHashtag) -> onClickListener.onClick(ivProfilePic));
        ivProfilePic.setOnClickListener(onClickListener);

        profileDialogAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,
                new String[]{resources.getString(R.string.open_profile), resources.getString(R.string.view_pfp)});

        postModel.setPosition(intent.getIntExtra(Constants.EXTRAS_INDEX, -1));
        postShortCode = postModel.getShortCode();

        final boolean postIdNull = postModel.getPostId() == null;
        if (!postIdNull)
            setupPostInfoBar(intent.getStringExtra(Constants.EXTRAS_USER), postModel.isVideo());

        isFromShare = postModel.getPosition() == -1 || postIdNull;

        btnDownload = findViewById(R.id.btnDownload);
        btnMute = findViewById(R.id.btnMute);
        btnComments = findViewById(R.id.btnComments);
        tvCommentsCount = btnComments.findViewById(R.id.commentsCount);
        tvPostDate = findViewById(R.id.tvPostDate);
        tvVideoViews = findViewById(R.id.tvVideoViews);
        viewsContainer = (View) tvVideoViews.getParent();
        mediaList = findViewById(R.id.mediaList);
        mediaList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        mediaList.setAdapter(mediaAdapter);

        viewerCaption = findViewById(R.id.viewerCaption);
        viewerCaptionParent = (View) viewerCaption.getParent();

        mediaList.setVisibility(View.GONE);
        swipeEvent = isRight -> {
            if (itemGetType != null && Main.itemGetter != null) {
                final List<? extends BasePostModel> itemGetterItems = Main.itemGetter.get(itemGetType);

                final boolean isMainSwipe = !(itemGetterItems.size() < 1 || itemGetType == ItemGetter.ItemGetType.MAIN_ITEMS && isFromShare);

                final BasePostModel[] basePostModels = mediaAdapter != null ? mediaAdapter.getPostModels() : null;
                final int slides = basePostModels != null ? basePostModels.length : 0;

                int position = postModel.getPosition();

                if (isRight) {
                    --slidePos;
                    if (!isMainSwipe && slidePos < 0) slidePos = 0;
                    if (slides > 0 && slidePos >= 0) {
                        if (basePostModels[slidePos] instanceof ViewerPostModel) {
                            viewerPostModel = (ViewerPostModel) basePostModels[slidePos];
                        }
                        refreshPost();
                        return;
                    }
                    if (isMainSwipe && --position < 0) position = itemGetterItems.size() - 1;
                } else {
                    ++slidePos;
                    if (!isMainSwipe && slidePos >= slides) slidePos = slides - 1;
                    if (slides > 0 && slidePos < slides) {
                        if (basePostModels[slidePos] instanceof ViewerPostModel) {
                            viewerPostModel = (ViewerPostModel) basePostModels[slidePos];
                        }
                        refreshPost();
                        return;
                    }
                    if (isMainSwipe && ++position >= itemGetterItems.size()) position = 0;
                }

                if (isMainSwipe) {
                    slidePos = 0;
                    Log.d("AWAISKING_APP", "swipe left <<< post[" + position + "]: " + postModel + " -- " + slides);
                    postModel = itemGetterItems.get(position);
                    postModel.setPosition(position);
                    viewPost();
                }
            }
        };
        gestureDetector = new GestureDetectorCompat(this, new SwipeListener(swipeEvent));

        viewPost();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void viewPost() {
        mediaList.setVisibility(View.GONE);
        mediaAdapter.setData(null);
        btnDownload.setVisibility(View.INVISIBLE);
        btnMute.setVisibility(View.GONE);
        tvPostDate.setVisibility(View.GONE);
        btnComments.setVisibility(View.GONE);
        viewsContainer.setVisibility(View.GONE);
        viewerCaptionParent.setVisibility(View.GONE);
        viewerCaption.setText(null);
        viewerCaption.setMentionClickListener(null);

        new PostFetcher(postModel.getShortCode(), result -> {
            if (result == null || result.length < 1) {
                Toast.makeText(this, R.string.downloader_unknown_error, Toast.LENGTH_SHORT).show();
                return;
            }

            playerView = findViewById(R.id.playerView);
            progressView = findViewById(R.id.progressView);
            photoView = findViewById(R.id.imageViewer);
            controller = findViewById(R.id.controller);

            final View.OnTouchListener gestureTouchListener = new View.OnTouchListener() {
                private float startX;
                private float startY;

                @Override
                public boolean onTouch(final View v, final MotionEvent event) {
                    if (v == viewerCaptionParent) {
                        switch (event.getAction()) {
                            case MotionEvent.ACTION_DOWN:
                                startX = event.getX();
                                startY = event.getY();
                                break;

                            case MotionEvent.ACTION_UP:
                                if (!(Utils.isEmpty(postCaption) ||
                                        Math.abs(startX - event.getX()) > 50 || Math.abs(startY - event.getY()) > 50)) {
                                    Utils.copyText(PostViewer.this, postCaption);
                                    return false;
                                }
                        }
                    }
                    return gestureDetector.onTouchEvent(event);
                }
            };
            viewerCaptionParent.setOnTouchListener(gestureTouchListener);
            playerView.setOnTouchListener(gestureTouchListener);
            photoView.setOnSingleFlingListener((e1, e2, velocityX, velocityY) -> {
                final float diffX = e2.getX() - e1.getX();
                if (Math.abs(diffX) > Math.abs(e2.getY() - e1.getY()) && Math.abs(diffX) > SwipeListener.SWIPE_THRESHOLD
                        && Math.abs(velocityX) > SwipeListener.SWIPE_VELOCITY_THRESHOLD) {
                    swipeEvent.onSwipe(diffX > 0);
                    return true;
                }
                return false;
            });

            mediaAdapter.setData(result);
            if (result.length > 1) {
                mediaList.setVisibility(View.VISIBLE);
            }

            viewerPostModel = (ViewerPostModel) result[0];

            final long commentsCount = viewerPostModel.getCommentsCount();
            tvCommentsCount.setText(String.valueOf(commentsCount));
            btnComments.setVisibility(View.VISIBLE);

            btnComments.setOnClickListener(commentsCount > 0 ? v -> startActivityForResult(new Intent(PostViewer.this, CommentsViewer.class)
                    .putExtra(Constants.EXTRAS_SHORTCODE, postShortCode), 6969) : null);

            if (postModel instanceof PostModel) {
                final PostModel postModel = (PostModel) this.postModel;
                postModel.setPostId(viewerPostModel.getPostId());
                postModel.setTimestamp(viewerPostModel.getTimestamp());
                postModel.setPostCaption(viewerPostModel.getPostCaption());
            }

            setupPostInfoBar(viewerPostModel.getUsername(), viewerPostModel.isVideo());

            postCaption = postModel.getPostCaption();
            viewerCaptionParent.setVisibility(View.VISIBLE);

            if (Utils.hasMentions(postCaption)) {
                viewerCaption.setText(Utils.getMentionText(postCaption), TextView.BufferType.SPANNABLE);
                viewerCaption.setMentionClickListener((text, isHashtag) -> new AlertDialog.Builder(PostViewer.this).setTitle(text)
                        .setMessage(isHashtag ? R.string.comment_view_mention_hash_search : R.string.comment_view_mention_user_search)
                        .setNegativeButton(R.string.cancel, null).setPositiveButton(R.string.ok, (dialog, which) -> searchUsername(text)).show());
            } else {
                viewerCaption.setText(postCaption);
            }

            btnDownload.setOnClickListener(v -> {
                if (ContextCompat.checkSelfPermission(this, Utils.PERMS[0]) == PackageManager.PERMISSION_GRANTED)
                    showDownloadDialog();
                else
                    ActivityCompat.requestPermissions(this, Utils.PERMS, 8020);
            });

            refreshPost();
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void searchUsername(final String text) {
        if (Main.scanHack != null) {
            Main.scanHack.onResult(text);
            finish();
        }
    }

    private void setupVideo() {
        btnDownload.setVisibility(View.VISIBLE);
        btnMute.setVisibility(View.VISIBLE);
        playerView.setVisibility(View.VISIBLE);
        viewsContainer.setVisibility(View.VISIBLE);
        progressView.setVisibility(View.GONE);
        photoView.setVisibility(View.GONE);
        photoView.setImageDrawable(null);
        if (controller != null) controller.setVisibility(View.GONE);

        tvVideoViews.setText(String.valueOf(viewerPostModel.getVideoViews()));

        player = new SimpleExoPlayer.Builder(this).build();
        playerView.setPlayer(player);
        float vol = Utils.sharedPreferences.getBoolean(Constants.MUTED_VIDEOS, true) ? 0f : 1f;
        if (vol == 0f && Utils.sessionVolumeFull) vol = 1f;

        player.setVolume(vol);
        player.setPlayWhenReady(Utils.sharedPreferences.getBoolean(Constants.AUTOPLAY_VIDEOS, true));
        final ProgressiveMediaSource mediaSource = new ProgressiveMediaSource.Factory(new DefaultDataSourceFactory(this, "instagram"))
                .createMediaSource(Uri.parse(url));
        mediaSource.addEventListener(new Handler(), new MediaSourceEventListener() {
            @Override
            public void onLoadCompleted(final int windowIndex, @Nullable final MediaSource.MediaPeriodId mediaPeriodId, final LoadEventInfo loadEventInfo, final MediaLoadData mediaLoadData) {
                progressView.setVisibility(View.GONE);
            }

            @Override
            public void onLoadStarted(final int windowIndex, @Nullable final MediaSource.MediaPeriodId mediaPeriodId, final LoadEventInfo loadEventInfo, final MediaLoadData mediaLoadData) {
                progressView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadCanceled(final int windowIndex, @Nullable final MediaSource.MediaPeriodId mediaPeriodId, final LoadEventInfo loadEventInfo, final MediaLoadData mediaLoadData) {
                progressView.setVisibility(View.GONE);
            }

            @Override
            public void onLoadError(final int windowIndex, @Nullable final MediaSource.MediaPeriodId mediaPeriodId, final LoadEventInfo loadEventInfo, final MediaLoadData mediaLoadData, final IOException error, final boolean wasCanceled) {
                progressView.setVisibility(View.GONE);
            }
        });
        player.prepare(mediaSource);

        player.setVolume(vol);
        btnMute.setImageResource(vol == 0f ? R.drawable.vol : R.drawable.mute);

        final View.OnClickListener onClickListener = v -> {
            if (player == null) return;
            if (v == playerView) {
                if (player.getPlaybackState() == Player.STATE_ENDED) player.seekTo(0);
                player.setPlayWhenReady(player.getPlaybackState() == Player.STATE_ENDED || !player.isPlaying());
            } else if (v == btnMute) {
                final float intVol = player.getVolume() == 0f ? 1f : 0f;
                player.setVolume(intVol);
                btnMute.setImageResource(intVol == 0f ? R.drawable.vol : R.drawable.mute);
                Utils.sessionVolumeFull = intVol == 1f;
            }
        };
        playerView.setOnClickListener(onClickListener);
        btnMute.setOnClickListener(onClickListener);
    }

    private void setupImage() {
        btnDownload.setVisibility(View.VISIBLE);
        btnMute.setVisibility(View.GONE);
        playerView.setVisibility(View.GONE);
        viewsContainer.setVisibility(View.GONE);
        progressView.setVisibility(View.VISIBLE);

        photoView.setImageDrawable(null);
        photoView.setVisibility(View.VISIBLE);
        photoView.setZoomable(true);
        photoView.setZoomTransitionDuration(420);
        photoView.setMaximumScale(7.2f);

        Picasso.get().load(url).into(photoView, new Callback() {
            @Override
            public void onSuccess() {
                progressView.setVisibility(View.GONE);
            }

            @Override
            public void onError(final Exception e) {
                progressView.setVisibility(View.GONE);
            }
        });
    }

    private void showDownloadDialog() {
        final ArrayList<BasePostModel> postModels = new ArrayList<>();

        if (!session && mediaList.getVisibility() == View.VISIBLE) {
            final DialogInterface.OnClickListener clickListener = (dialog, which) -> {
                postModels.clear();

                if (which == DialogInterface.BUTTON_NEGATIVE) {
                    final BasePostModel[] adapterPostModels = mediaAdapter.getPostModels();
                    for (int i = 0, size = mediaAdapter.getItemCount(); i < size; ++i) {
                        if (adapterPostModels[i] instanceof ViewerPostModel)
                            postModels.add(adapterPostModels[i]);
                    }
                } else if (which == DialogInterface.BUTTON_POSITIVE) {
                    postModels.add(viewerPostModel);
                } else {
                    session = true;
                    postModels.add(viewerPostModel);
                }

                if (postModels.size() > 0)
                    Utils.batchDownload(this, viewerPostModel.getUsername(), false, postModels);
            };

            new AlertDialog.Builder(this).setTitle(R.string.post_viewer_download_dialog_title)
                    .setMessage(R.string.post_viewer_download_message)
                    .setNeutralButton(R.string.post_viewer_download_session, clickListener).setPositiveButton(R.string.post_viewer_download_current, clickListener)
                    .setNegativeButton(R.string.post_viewer_download_album, clickListener).show();
        } else {
            Utils.batchDownload(this, viewerPostModel.getUsername(), false, Collections.singletonList(viewerPostModel));
        }
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 8020 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            showDownloadDialog();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 6969) {
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Build.VERSION.SDK_INT < 24) releasePlayer();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Build.VERSION.SDK_INT >= 24) releasePlayer();
    }

    private void refreshPost() {
        postShortCode = viewerPostModel.getShortCode();

        setupPostInfoBar(viewerPostModel.getUsername(), viewerPostModel.isVideo());

        if (postModel instanceof PostModel) {
            final PostModel postModel = (PostModel) this.postModel;
            postModel.setPostId(viewerPostModel.getPostId());
            postModel.setTimestamp(viewerPostModel.getTimestamp());
            postModel.setPostCaption(viewerPostModel.getPostCaption());
        }

        tvPostDate.setText(viewerPostModel.getPostDate());
        tvPostDate.setVisibility(View.VISIBLE);
        tvPostDate.setSelected(true);

        url = viewerPostModel.getDisplayUrl();
        releasePlayer();
        if (viewerPostModel.isVideo()) setupVideo();
        else setupImage();
    }

    private void releasePlayer() {
        if (player == null) return;
        player.release();
        player = null;
    }

    private void setupPostInfoBar(final String from, final boolean isVideo) {
        if (prevUsername == null || !prevUsername.equals(from)) {
            ivProfilePic.setImageBitmap(null);
            ivProfilePic.setImageDrawable(null);
            ivProfilePic.setImageResource(0);

            if (from.charAt(0) != '#')
                new ProfileFetcher(from, result -> {
                    profileModel = result;

                    if (result != null) {
                        final String hdProfilePic = result.getHdProfilePic();
                        final String sdProfilePic = result.getSdProfilePic();

                        final boolean hdPicEmpty = Utils.isEmpty(hdProfilePic);
                        Picasso.get().load(hdPicEmpty ? sdProfilePic : hdProfilePic).into(ivProfilePic, new Callback() {
                            private boolean loaded = true;

                            @Override
                            public void onSuccess() {
                                ivProfilePic.setEnabled(true);
                                ivProfilePic.setOnClickListener(onClickListener);
                            }

                            @Override
                            public void onError(final Exception e) {
                                ivProfilePic.setEnabled(false);
                                ivProfilePic.setOnClickListener(null);
                                if (loaded) {
                                    loaded = false;
                                    if (!Utils.isEmpty(sdProfilePic))
                                        Picasso.get().load(sdProfilePic).into(ivProfilePic, this);
                                }
                            }
                        });
                    }
                }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            prevUsername = from;
        }

        if (title != null) {
            final String titlePrefix = resources.getString(isVideo ? R.string.post_viewer_video_post : R.string.post_viewer_image_post);
            if (Utils.isEmpty(from)) title.setText(titlePrefix);
            else {
                final CharSequence titleText = resources.getString(R.string.post_viewer_post_from, titlePrefix, from) + " ";
                final int titleLen = titleText.length();
                final SpannableString spannableString = new SpannableString(titleText);
                spannableString.setSpan(new CommentMentionClickSpan(), titleLen - from.length() - 1, titleLen - 1, 0);
                title.setText(spannableString);
            }
        }
    }
}