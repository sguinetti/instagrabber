package awais.instagrabber.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;

import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;

import awais.instagrabber.R;
import awais.instagrabber.asyncs.DownloadAsync;
import awais.instagrabber.asyncs.ProfilePictureFetcher;
import awais.instagrabber.dialogs.ProfileSettingsDialog;
import awais.instagrabber.interfaces.FetchListener;
import awais.instagrabber.models.ProfileModel;
import awais.instagrabber.utils.Constants;
import awais.instagrabber.utils.Utils;

public class ProfileViewer extends AppCompatActivity {
    private ProfileModel profileModel;
    private MenuItem menuItemDownload;
    private String profilePicUrl;
    private FragmentManager fragmentManager;
    private FetchListener<String> fetchListener;
    private boolean errorHandled = false;
    private boolean fallbackToProfile = false;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final Intent intent = getIntent();
        if (intent == null || !intent.hasExtra(Constants.EXTRAS_PROFILE)
                || (profileModel = (ProfileModel) intent.getSerializableExtra(Constants.EXTRAS_PROFILE)) == null) {
            Utils.errorFinish(this);
            return;
        }

        fragmentManager = getSupportFragmentManager();

        final String id = profileModel.getId();
        final String username = profileModel.getUsername();

        toolbar.setTitle(username);

        final View progressView = findViewById(R.id.progressView);
        final PhotoView photoView = findViewById(R.id.imageViewer);
        final TextView imageInfo = findViewById(R.id.imageInfo);

        progressView.setVisibility(View.VISIBLE);
        photoView.setVisibility(View.VISIBLE);

        photoView.setZoomable(true);
        photoView.setZoomTransitionDuration(420);
        photoView.setMaximumScale(7.2f);

        final boolean isAlt = Utils.sharedPreferences.getBoolean(Constants.USE_ALTER_PROFILE, false);

        fetchListener = profileUrl -> {
            profilePicUrl = profileUrl;

            if (!fallbackToProfile && Utils.isEmpty(profilePicUrl)) {
                fallbackToProfile = true;
                new ProfilePictureFetcher(username, id, fetchListener, !isAlt).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                return;
            }

            if (errorHandled && fallbackToProfile || Utils.isEmpty(profilePicUrl))
                profilePicUrl = profileModel.getHdProfilePic();

            Picasso.get().load(profilePicUrl).into(photoView, new Callback() {
                @Override
                public void onSuccess() {
                    if (menuItemDownload != null) menuItemDownload.setEnabled(true);
                    showImageInfo();
                    progressView.setVisibility(View.GONE);
                }

                @Override
                public void onError(final Exception e) {
                    fallbackToProfile = true;
                    if (!errorHandled) {
                        errorHandled = true;
                        new ProfilePictureFetcher(username, id, fetchListener, !isAlt).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        Picasso.get().load(profileModel.getHdProfilePic()).into(photoView);
                        showImageInfo();
                    }
                    progressView.setVisibility(View.GONE);
                }

                private void showImageInfo() {
                    final Drawable drawable = photoView.getDrawable();
                    if (drawable != null) {
                        final StringBuilder info = new StringBuilder(getString(R.string.profile_viewer_imageinfo, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight()));
                        if (drawable instanceof BitmapDrawable) {
                            final Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                            if (bitmap != null) {
                                final String colorDepthPrefix = getString(R.string.profile_viewer_colordepth_prefix);
                                switch (bitmap.getConfig()) {
                                    case ALPHA_8:
                                        info.append(colorDepthPrefix).append(" 8-bits(A)");
                                        break;
                                    case RGB_565:
                                        info.append(colorDepthPrefix).append(" 16-bits-A");
                                        break;
                                    case ARGB_4444:
                                        info.append(colorDepthPrefix).append(" 16-bits+A");
                                        break;
                                    case ARGB_8888:
                                        info.append(colorDepthPrefix).append(" 32-bits+A");
                                        break;
                                    case RGBA_F16:
                                        info.append(colorDepthPrefix).append(" 64-bits+A");
                                        break;
                                    case HARDWARE:
                                        info.append(colorDepthPrefix).append(" auto");
                                        break;
                                }
                            }
                        }
                        imageInfo.setText(info);
                        imageInfo.setVisibility(View.VISIBLE);
                    }
                }
            });
        };

        new ProfilePictureFetcher(username, isAlt ? id : null, fetchListener, isAlt).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);

        final MenuItem.OnMenuItemClickListener menuItemClickListener = item -> {
            if (item == menuItemDownload) {
                if (profileModel == null) {
                    Toast.makeText(this, R.string.downloader_unknown_error, Toast.LENGTH_SHORT).show();
                    return true;
                }

                final File dir = new File(Environment.getExternalStorageDirectory(), "Download");
                if (!dir.exists() && !dir.mkdirs()) {
                    Toast.makeText(this, R.string.downloader_error_creating_folder, Toast.LENGTH_SHORT).show();
                    return true;
                }

                final int index = profilePicUrl.indexOf('?');
                new DownloadAsync(this, profilePicUrl, new File(dir, profileModel.getUsername() + '_' +
                        System.currentTimeMillis() + profilePicUrl.substring(index - 4, index)), result -> {
                    final int toastRes = result != null && result.exists() ? R.string.downloader_downloaded_in_folder
                            : R.string.downloader_error_download_file;
                    Toast.makeText(this, toastRes, Toast.LENGTH_SHORT).show();
                }).setItems(null, profileModel.getUsername()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new ProfileSettingsDialog().show(fragmentManager, "settings");
            }
            return true;
        };

        menu.findItem(R.id.action_search).setVisible(false);
        menuItemDownload = menu.findItem(R.id.action_download);
        menuItemDownload.setVisible(true);
        menuItemDownload.setEnabled(false);
        menuItemDownload.setOnMenuItemClickListener(menuItemClickListener);

        final MenuItem menuItemSettings = menu.findItem(R.id.action_settings);
        menuItemSettings.setVisible(true);
        menuItemSettings.setOnMenuItemClickListener(menuItemClickListener);

        return true;
    }
}