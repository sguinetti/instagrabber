package awais.instagrabber.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GestureDetectorCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MediaSourceEventListener;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;

import awais.instagrabber.BuildConfig;
import awais.instagrabber.R;
import awais.instagrabber.adapters.StoriesAdapter;
import awais.instagrabber.asyncs.DownloadAsync;
import awais.instagrabber.interfaces.SwipeEvent;
import awais.instagrabber.models.PostModel;
import awais.instagrabber.models.StoryModel;
import awais.instagrabber.utils.Constants;
import awais.instagrabber.utils.SwipeListener;
import awais.instagrabber.utils.Utils;
import awaisomereport.LogCollector;

import static awais.instagrabber.utils.SwipeListener.SWIPE_THRESHOLD;
import static awais.instagrabber.utils.SwipeListener.SWIPE_VELOCITY_THRESHOLD;
import static awais.instagrabber.utils.Utils.logCollector;

public final class StoryViewer extends AppCompatActivity {
    private final StoriesAdapter storiesAdapter = new StoriesAdapter(null, new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            final Object tag = v.getTag();
            if (tag instanceof StoryModel) {
                currentStory = (StoryModel) tag;
                slidePos = currentStory.getPosition();
                refreshStory();
            }
        }
    });
    private StoryModel[] storyModels;
    private GestureDetectorCompat gestureDetector;
    private RecyclerView storiesList;
    private SimpleExoPlayer player;
    private PlayerView playerView;
    private SwipeEvent swipeEvent;
    private PhotoView photoView;
    private View progressView, viewStoryPost;
    private MenuItem menuDownload;
    private StoryModel currentStory;
    private String url;
    private int slidePos = 0;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story_viewer);

        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final Intent intent = getIntent();
        if (intent == null || !intent.hasExtra(Constants.EXTRAS_STORIES)) {
            Utils.errorFinish(this);
            return;
        }

        storyModels = (StoryModel[]) intent.getSerializableExtra(Constants.EXTRAS_STORIES);

        final String username = intent.getStringExtra(Constants.EXTRAS_USERNAME);
        final String highlight = intent.getStringExtra(Constants.EXTRAS_HIGHLIGHT);
        final boolean hasUsername = !Utils.isEmpty(username);
        final boolean hasHighlight = !Utils.isEmpty(highlight);

        if (hasUsername) {
            toolbar.setTitle(username);
            if (hasHighlight) toolbar.setSubtitle(getString(R.string.title_highlight, highlight));
            else toolbar.setSubtitle(R.string.title_user_story);
        }

        storiesList = findViewById(R.id.storiesList);
        storiesList.setVisibility(View.GONE);
        storiesList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        storiesList.setAdapter(storiesAdapter);

        swipeEvent = new SwipeEvent() {
            private final int storiesLen = storyModels != null ? storyModels.length : 0;

            @Override
            public void onSwipe(final boolean isRightSwipe) {
                if (storyModels != null && storiesLen > 0) {
                    if (isRightSwipe) {
                        if (--slidePos <= 0) slidePos = 0;
                    } else if (++slidePos >= storiesLen) slidePos = storiesLen - 1;

                    currentStory = storyModels[slidePos];
                    slidePos = currentStory.getPosition();
                    refreshStory();
                }
            }
        };
        gestureDetector = new GestureDetectorCompat(this, new SwipeListener(swipeEvent));

        viewPost();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void viewPost() {
        storiesList.setVisibility(View.GONE);
        storiesAdapter.setData(null);

        if (menuDownload != null) menuDownload.setVisible(false);

        photoView = findViewById(R.id.imageViewer);
        playerView = findViewById(R.id.playerView);
        viewStoryPost = findViewById(R.id.viewStoryPost);
        progressView = findViewById(R.id.progressView);

        playerView.setOnTouchListener((v, event) -> gestureDetector.onTouchEvent(event));
        photoView.setOnSingleFlingListener((e1, e2, velocityX, velocityY) -> {
            try {
                final float diffX = e2.getX() - e1.getX();
                if (Math.abs(diffX) > Math.abs(e2.getY() - e1.getY()) && Math.abs(diffX) > SWIPE_THRESHOLD
                        && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                    swipeEvent.onSwipe(diffX > 0);
                    return true;
                }
            } catch (final Exception e) {
                if (logCollector != null)
                    logCollector.appendException(e, LogCollector.LogFile.ACTIVITY_STORY_VIEWER, "viewPost");
                if (BuildConfig.DEBUG) Log.e("AWAISKING_APP", "", e);
            }
            return false;
        });

        viewStoryPost.setOnClickListener(v -> {
            final Object tag = v.getTag();
            if (tag instanceof CharSequence) startActivity(new Intent(this, PostViewer.class)
                    .putExtra(Constants.EXTRAS_POST, new PostModel(tag.toString())));
        });

        storiesAdapter.setData(storyModels);
        if (storyModels.length > 1) storiesList.setVisibility(View.VISIBLE);

        currentStory = storyModels[0];
        refreshStory();
    }

    private void setupVideo() {
        playerView.setVisibility(View.VISIBLE);
        progressView.setVisibility(View.GONE);
        photoView.setVisibility(View.GONE);
        photoView.setImageDrawable(null);

        player = new SimpleExoPlayer.Builder(this).build();
        playerView.setPlayer(player);
        player.setPlayWhenReady(Utils.sharedPreferences.getBoolean(Constants.AUTOPLAY_VIDEOS, true));

        final ProgressiveMediaSource mediaSource = new ProgressiveMediaSource.Factory(new DefaultDataSourceFactory(this, "instagram"))
                .createMediaSource(Uri.parse(url));
        mediaSource.addEventListener(new Handler(), new MediaSourceEventListener() {
            @Override
            public void onLoadCompleted(final int windowIndex, @Nullable final MediaSource.MediaPeriodId mediaPeriodId, final LoadEventInfo loadEventInfo, final MediaLoadData mediaLoadData) {
                if (menuDownload != null) menuDownload.setVisible(true);
                progressView.setVisibility(View.GONE);
            }

            @Override
            public void onLoadStarted(final int windowIndex, @Nullable final MediaSource.MediaPeriodId mediaPeriodId, final LoadEventInfo loadEventInfo, final MediaLoadData mediaLoadData) {
                if (menuDownload != null) menuDownload.setVisible(true);
                progressView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadCanceled(final int windowIndex, @Nullable final MediaSource.MediaPeriodId mediaPeriodId, final LoadEventInfo loadEventInfo, final MediaLoadData mediaLoadData) {
                progressView.setVisibility(View.GONE);
            }

            @Override
            public void onLoadError(final int windowIndex, @Nullable final MediaSource.MediaPeriodId mediaPeriodId, final LoadEventInfo loadEventInfo, final MediaLoadData mediaLoadData, final IOException error, final boolean wasCanceled) {
                if (menuDownload != null) menuDownload.setVisible(false);
                progressView.setVisibility(View.GONE);
            }
        });
        player.prepare(mediaSource);

        playerView.setOnClickListener(v -> {
            if (player != null) {
                if (player.getPlaybackState() == Player.STATE_ENDED) player.seekTo(0);
                player.setPlayWhenReady(player.getPlaybackState() == Player.STATE_ENDED || !player.isPlaying());
            }
        });
    }

    private void setupImage() {
        progressView.setVisibility(View.VISIBLE);
        playerView.setVisibility(View.GONE);

        photoView.setImageDrawable(null);
        photoView.setVisibility(View.VISIBLE);
        photoView.setZoomable(true);
        photoView.setZoomTransitionDuration(420);
        photoView.setMaximumScale(7.2f);

        Picasso.get().load(url).into(photoView, new Callback() {
            @Override
            public void onSuccess() {
                if (menuDownload != null) menuDownload.setVisible(true);
                progressView.setVisibility(View.GONE);
            }

            @Override
            public void onError(final Exception e) {
                progressView.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);

        menu.findItem(R.id.action_settings).setVisible(false);
        menu.findItem(R.id.action_search).setVisible(false);

        menuDownload = menu.findItem(R.id.action_download);
        menuDownload.setVisible(true);
        menuDownload.setOnMenuItemClickListener(item -> {
            if (ContextCompat.checkSelfPermission(this, Utils.PERMS[0]) == PackageManager.PERMISSION_GRANTED)
                downloadStory();
            else
                ActivityCompat.requestPermissions(this, Utils.PERMS, 8020);
            return true;
        });

        return true;
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 8020 && grantResults[0] == PackageManager.PERMISSION_GRANTED) downloadStory();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Build.VERSION.SDK_INT < 24) releasePlayer();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (Build.VERSION.SDK_INT >= 24) releasePlayer();
    }

    private void downloadStory() {
        int error = 0;
        if (currentStory != null) {
            final File dir = new File(Environment.getExternalStorageDirectory(), "Download");
            if (dir.exists() || dir.mkdirs()) {
                final String storyUrl = currentStory.isVideo() ? currentStory.getVideoUrl() : currentStory.getStoryUrl();
                final int index = storyUrl.indexOf('?');
                final File file = new File(dir, currentStory.getStoryMediaId() + "_" + currentStory.getTimestamp()
                        + storyUrl.substring(index - 4, index));

                new DownloadAsync(this, storyUrl, file, result -> {
                    final int toastRes = result != null && result.exists() ? R.string.downloader_error_download_file
                            : R.string.downloader_complete;
                    Toast.makeText(this, toastRes, Toast.LENGTH_SHORT).show();
                }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else error = 1;
        } else error = 2;

        if (error == 1) Toast.makeText(this, R.string.downloader_error_creating_folder, Toast.LENGTH_SHORT).show();
        else if (error == 2) Toast.makeText(this, R.string.downloader_unknown_error, Toast.LENGTH_SHORT).show();
    }

    private void refreshStory() {
        if (menuDownload != null) menuDownload.setVisible(false);
        url = currentStory.isVideo() ? currentStory.getVideoUrl() : currentStory.getStoryUrl();

        if (viewStoryPost != null) {
            final String shortCode = currentStory.getTappableShortCode();
            viewStoryPost.setVisibility(shortCode != null ? View.VISIBLE : View.GONE);
            viewStoryPost.setTag(shortCode);
        }

        releasePlayer();
        if (currentStory.isVideo()) setupVideo();
        else setupImage();
    }

    private void releasePlayer() {
        if (player != null) {
            try { player.stop(true); } catch (Exception ignored) { }
            try { player.release(); } catch (Exception ignored) { }
            player = null;
        }
    }
}