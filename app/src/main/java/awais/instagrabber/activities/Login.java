package awais.instagrabber.activities;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import awais.instagrabber.R;
import awais.instagrabber.utils.Constants;
import awais.instagrabber.utils.Utils;

public final class Login extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private final WebViewClient webViewClient = new WebViewClient() {
        @Override
        public void onPageStarted(final WebView view, final String url, final Bitmap favicon) {
            webViewUrl = url;
        }

        @Override
        public void onPageFinished(final WebView view, final String url) {
            webViewUrl = url;
        }
    };
    private final WebChromeClient webChromeClient = new WebChromeClient();
    private String webViewUrl, defaultUserAgent;
    private Button btnCookies, btnRefresh;
    private WebView webView;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        webView = findViewById(R.id.webView);
        btnCookies = findViewById(R.id.cookies);
        btnRefresh = findViewById(R.id.refresh);

        initWebView();

        ((CheckBox) findViewById(R.id.desktop_mode)).setOnCheckedChangeListener(this);

        btnRefresh.setOnClickListener(this);
        btnCookies.setOnClickListener(this);
    }

    @Override
    public void onClick(final View v) {
        if (v == btnRefresh) {
            if (webView != null) webView.loadUrl("https://instagram.com/");
        } else if (v == btnCookies) {
            final String mainCookie = Utils.getCookie(webViewUrl);
            if (Utils.isEmpty(mainCookie))
                Toast.makeText(this, R.string.login_error_loading_cookies, Toast.LENGTH_SHORT).show();
            else {
                Utils.setupCookies(mainCookie);
                Utils.sharedPreferences.edit().putString(Constants.COOKIE, mainCookie).apply();
                Toast.makeText(this, R.string.login_success_loading_cookies, Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    @Override
    public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
        if (webView != null) {
            final WebSettings webSettings = webView.getSettings();

            final String newUserAgent = isChecked ? "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36"
                    : defaultUserAgent;

            webSettings.setUserAgentString(newUserAgent);
            webSettings.setUseWideViewPort(isChecked);
            webSettings.setLoadWithOverviewMode(isChecked);
            webSettings.setSupportZoom(isChecked);
            webSettings.setBuiltInZoomControls(isChecked);

            webView.loadUrl("https://instagram.com/");
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initWebView() {
        if (webView != null) {
            webView.setWebChromeClient(webChromeClient);
            webView.setWebViewClient(webViewClient);
            final WebSettings webSettings = webView.getSettings();
            if (webSettings != null) {
                if (defaultUserAgent == null) defaultUserAgent = webSettings.getUserAgentString();
                webSettings.setJavaScriptEnabled(true);
                webSettings.setDomStorageEnabled(true);
                webSettings.setSupportZoom(true);
                webSettings.setBuiltInZoomControls(true);
                webSettings.setDisplayZoomControls(false);
                webSettings.setLoadWithOverviewMode(true);
                webSettings.setUseWideViewPort(true);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    webSettings.setAllowFileAccessFromFileURLs(true);
                    webSettings.setAllowUniversalAccessFromFileURLs(true);
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            }

            webView.loadUrl("https://instagram.com/");
        }
    }

    @Override
    protected void onPause() {
        if (webView != null) webView.onPause();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (webView != null) webView.onResume();
    }

    @Override
    protected void onDestroy() {
        if (webView != null) webView.destroy();
        super.onDestroy();
    }
}