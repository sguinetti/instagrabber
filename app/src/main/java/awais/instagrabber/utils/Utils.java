package awais.instagrabber.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.MimeTypeMap;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FilenameFilter;
import java.io.InputStreamReader;
import java.net.CookiePolicy;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import awais.instagrabber.BuildConfig;
import awais.instagrabber.R;
import awais.instagrabber.activities.Main;
import awais.instagrabber.asyncs.DownloadAsync;
import awais.instagrabber.asyncs.PostFetcher;
import awais.instagrabber.customviews.CommentMentionClickSpan;
import awais.instagrabber.models.BasePostModel;
import awais.instagrabber.models.IntentModel;
import awaisomereport.LogCollector;

import static awais.instagrabber.utils.Constants.FOLDER_PATH;
import static awais.instagrabber.utils.Constants.FOLDER_SAVE_TO;

public final class Utils {
    public static LogCollector logCollector;
    public static SharedPreferences sharedPreferences;
    public static DataBox dataBox;
    public static boolean sessionVolumeFull = false;
    @SuppressLint("StaticFieldLeak")
    public static NotificationManagerCompat notificationManager;
    public static final CookieManager COOKIE_MANAGER = CookieManager.getInstance();
    public static final String[] PERMS = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
    public static final java.net.CookieManager NET_COOKIE_MANAGER = new java.net.CookieManager(null, CookiePolicy.ACCEPT_ALL);
    public static final MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
    public static final String CHANNEL_ID = "InstaGrabber", CHANNEL_NAME = "Instagrabber",
            NOTIF_GROUP_NAME = "awais.instagrabber.InstaNotif";
    public static boolean isChannelCreated = false;
    public static boolean isInstagramInstalled = false;
    public static String telegramPackage;
    public static ClipboardManager clipboardManager;
    public static DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
    public static SimpleDateFormat datetimeParser;

    public static void setupCookies(final String cookieRaw) {
        if (cookieRaw != null && !isEmpty(cookieRaw)) {
            final CookieStore cookieStore = NET_COOKIE_MANAGER.getCookieStore();
            try {
                final URI uri1 = new URI("https://instagram.com");
                final URI uri2 = new URI("https://instagram.com/");
                for (final String cookie : cookieRaw.split(";")) {
                    final String[] strings = cookie.split("=", 2);
                    final HttpCookie httpCookie = new HttpCookie(strings[0].trim(), strings[1].trim());
                    httpCookie.setDomain("instagram.com");
                    httpCookie.setPath("/");
                    httpCookie.setVersion(0);
                    cookieStore.add(uri1, httpCookie);
                    cookieStore.add(uri2, httpCookie);
                }
            } catch (final URISyntaxException e) {
                if (logCollector != null)
                    logCollector.appendException(e, LogCollector.LogFile.UTILS, "setupCookies");
                if (BuildConfig.DEBUG) Log.e("AWAISKING_APP", "", e);
            }
        }
    }

    @Nullable
    public static String getUserIdFromCookie(final String cookie) {
        if (!isEmpty(cookie)) {
            final int uidIndex = cookie.indexOf("ds_user_id");
            if (uidIndex > 0) {
                final int uidEndIndex = cookie.indexOf(';', uidIndex + 10);
                if (uidEndIndex > 0) {
                    final String uid = cookie.substring(uidIndex + 11, uidEndIndex);
                    return !isEmpty(uid) ? uid : null;
                }
            }
        }
        return null;
    }

    @Nullable
    public static IntentModel stripString(@NonNull String clipString) {
        final int wwwDel = clipString.contains("www.") ? 4 : 0;
        final boolean isHttps = clipString.startsWith("https");

        IntentModel.IntentModelType type = IntentModel.IntentModelType.UNKNOWN;
        if (clipString.contains("instagram.com/")) {
            clipString = clipString.substring((isHttps ? 22 : 21) + wwwDel);

            final char firstChar = clipString.charAt(0);
            if ((firstChar == 'p' || firstChar == 'P') && clipString.charAt(1) == '/') {
                clipString = clipString.substring(2);
                type = IntentModel.IntentModelType.POST;
            } else if (clipString.startsWith("explore/tags/")) {
                clipString = clipString.substring(13);
                type = IntentModel.IntentModelType.HASHTAG;
            }

            clipString = cleanString(clipString);
        } else if (clipString.contains("ig.me/u/")) {
            clipString = clipString.substring((isHttps ? 16 : 15) + wwwDel);
            clipString = cleanString(clipString);
            type = IntentModel.IntentModelType.USERNAME;

        } else return null;

        final int clipLen = clipString.length() - 1;
        if (clipString.charAt(clipLen) == '/')
            clipString = clipString.substring(0, clipLen);

        return new IntentModel(type, clipString);
    }

    @NonNull
    public static String cleanString(@NonNull final String clipString) {
        final int queryIndex = clipString.indexOf('?');
        final int paramIndex = clipString.indexOf('#');
        int startIndex = -1;
        if (queryIndex > 0 && paramIndex > 0) {
            if (queryIndex < paramIndex) startIndex = queryIndex;
            else if (paramIndex < queryIndex) startIndex = paramIndex;
        } else if (queryIndex == -1 && paramIndex > 0) startIndex = paramIndex;
        else if (paramIndex == -1 && queryIndex > 0) startIndex = queryIndex;
        return startIndex != -1 ? clipString.substring(0, startIndex) : clipString;
    }

    @NonNull
    public static CharSequence getMentionText(@NonNull final CharSequence text) {
        final int commentLength = text.length();
        final SpannableStringBuilder stringBuilder = new SpannableStringBuilder(text, 0, commentLength);

        for (int i = 0; i < commentLength; ++i) {
            char currChar = text.charAt(i);

            if (currChar == '@' || currChar == '#') {
                final int startLen = i;

                do {
                    if (++i == commentLength) break;
                    currChar = text.charAt(i);

                    // for merged hashtags
                    if (currChar == '#') {
                        --i;
                        break;
                    }
                } while (currChar != ' ' && currChar != '\r' && currChar != '\n');

                stringBuilder.setSpan(new CommentMentionClickSpan(), startLen,
                        currChar != '#' ? i : i + 1, // for merged hashtags
                        Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            }
        }

        return stringBuilder;
    }

    @Nullable
    public static String getHighQualityPost(final JSONArray resources, final boolean isVideo) {
        try {
            final int resourcesLen = resources.length();

            final String[] sources = new String[resourcesLen];
            int lastResMain = 0, lastIndexMain = -1;
            int lastResBase = 0, lastIndexBase = -1;
            for (int i = 0; i < resourcesLen; ++i) {
                final JSONObject item = resources.getJSONObject(i);
                if (item != null && (!isVideo || item.has(Constants.EXTRAS_PROFILE))) {
                    sources[i] = item.getString("src");
                    final int currRes = item.getInt("config_width") * item.getInt("config_height");

                    final String profile = isVideo ? item.getString(Constants.EXTRAS_PROFILE) : null;

                    if (!isVideo || "MAIN".equals(profile)) {
                        if (currRes > lastResMain) {
                            lastResMain = currRes;
                            lastIndexMain = i;
                        }
                    } else if ("BASELINE".equals(profile)) {
                        if (currRes > lastResBase) {
                            lastResBase = currRes;
                            lastIndexBase = i;
                        }
                    }
                }
            }

            if (lastIndexMain >= 0) return sources[lastIndexMain];
            else if (lastIndexBase >= 0) return sources[lastIndexBase];
        } catch (final Exception e) {
            if (logCollector != null)
                logCollector.appendException(e, LogCollector.LogFile.UTILS, "getHighQualityPost");
            if (BuildConfig.DEBUG) Log.e("AWAISKING_APP", "", e);
        }
        return null;
    }

    public static String getHighQualityImage(final JSONObject resources) {
        String src = null;
        try {
            if (resources.has("display_resources")) src = getHighQualityPost(resources.getJSONArray("display_resources"), false);
            if (src == null) return resources.getString("display_url");
        } catch (final Exception e) {
            if (logCollector != null)
                logCollector.appendException(e, LogCollector.LogFile.UTILS, "getHighQualityImage");
            if (BuildConfig.DEBUG) Log.e("AWAISKING_APP", "", e);
        }
        return src;
    }

    public static String getDiscoverItemThumbnail(@NonNull final JSONArray jsonArray) {
        String thumbnail = null;
        final int imageResLen = jsonArray.length();

        for (int i = 0; i < imageResLen; ++i) {
            final JSONObject imageResource = jsonArray.optJSONObject(i);
            try {
                final int width = imageResource.getInt("width");
                final int height = imageResource.getInt("height");
                final float ratio = Float.parseFloat(String.format(Locale.getDefault(), "%.2f", (float) height / width));
                if (ratio >= 0.95f && ratio <= 1.0f) {
                    thumbnail = imageResource.getString("url");
                    break;
                }
            } catch (final Exception e) {
                if (logCollector != null)
                    logCollector.appendException(e, LogCollector.LogFile.UTILS, "getDiscoverItemThumbnail");
                if (BuildConfig.DEBUG) Log.e("AWAISKING_APP", "", e);
                thumbnail = null;
            }
        }

        if (Utils.isEmpty(thumbnail)) thumbnail = jsonArray.optJSONObject(0).optString("url");

        return thumbnail;
    }

    public static int convertDpToPx(final float dp) {
        if (displayMetrics == null)
            displayMetrics = Resources.getSystem().getDisplayMetrics();
        return Math.round((dp * displayMetrics.densityDpi) / 160.0f);
    }

    public static void changeTheme() {
        int themeCode = AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM; // this is fallback / default
        if (sharedPreferences != null) {
            themeCode = sharedPreferences.getInt(Constants.APP_THEME, themeCode);
            if (themeCode == 1) themeCode = AppCompatDelegate.MODE_NIGHT_AUTO_BATTERY;
            else if (themeCode == 3) themeCode = AppCompatDelegate.MODE_NIGHT_NO;
            else if (themeCode != 2) themeCode = AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM;
        }
        if (themeCode == AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM && Build.VERSION.SDK_INT < 29)
            themeCode = AppCompatDelegate.MODE_NIGHT_AUTO_BATTERY;
        AppCompatDelegate.setDefaultNightMode(themeCode);
    }

    public static void setTooltipText(final View view, @StringRes final int tooltipTextRes) {
        if (view != null && tooltipTextRes != 0 && tooltipTextRes != -1) {
            final Context context = view.getContext();
            final String tooltipText = context.getResources().getString(tooltipTextRes);

            if (Build.VERSION.SDK_INT >= 26) view.setTooltipText(tooltipText);
            else view.setOnLongClickListener(v -> {
                Toast.makeText(context, tooltipText, Toast.LENGTH_SHORT).show();
                return true;
            });
        }
    }

    // extracted from String class
    public static int indexOfChar(@NonNull final CharSequence sequence, final int ch, final int startIndex) {
        final int max = sequence.length();
        if (startIndex < max) {
            if (ch < Character.MIN_SUPPLEMENTARY_CODE_POINT) {
                for (int i = startIndex; i < max; i++) if (sequence.charAt(i) == ch) return i;
            } else if (Character.isValidCodePoint(ch)) {
                final char hi = (char) ((ch >>> 10) + (Character.MIN_HIGH_SURROGATE - (Character.MIN_SUPPLEMENTARY_CODE_POINT >>> 10)));
                final char lo = (char) ((ch & 0x3ff) + Character.MIN_LOW_SURROGATE);
                for (int i = startIndex; i < max; i++)
                    if (sequence.charAt(i) == hi && sequence.charAt(i + 1) == lo) return i;
            }
        }
        return -1;
    }

    public static boolean hasMentions(final CharSequence text) {
        if (isEmpty(text)) return false;
        return Utils.indexOfChar(text, '@', 0) != -1 || Utils.indexOfChar(text, '#', 0) != -1;
    }

    public static void copyText(final Context context, final CharSequence string) {
        final boolean ctxNotNull = context != null;
        if (ctxNotNull && clipboardManager == null)
            clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);

        int toastMessage = R.string.clipboard_error;
        if (clipboardManager != null) {
            clipboardManager.setPrimaryClip(ClipData.newPlainText(Utils.CHANNEL_NAME, string));
            toastMessage = R.string.clipboard_copied;
        }
        if (ctxNotNull) Toast.makeText(context, toastMessage, Toast.LENGTH_SHORT).show();
    }

    @NonNull
    public static String readFromConnection(@NonNull final HttpURLConnection conn) throws Exception {
        final StringBuilder sb = new StringBuilder();
        try (final BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {
            String line;
            while ((line = br.readLine()) != null) sb.append(line).append('\n');
        }
        return sb.toString();
    }

    public static void batchDownload(@NonNull final Context context, @Nullable final String username, final boolean isFromFeed,
                                     final List<? extends BasePostModel> itemsToDownload) {
        if (sharedPreferences == null)
            sharedPreferences = context.getSharedPreferences("settings", Context.MODE_PRIVATE);

        if (itemsToDownload == null || itemsToDownload.size() < 1) return;

        if (ContextCompat.checkSelfPermission(context, Utils.PERMS[0]) == PackageManager.PERMISSION_GRANTED)
            batchDownloadImpl(context, username, isFromFeed, itemsToDownload);
        else if (context instanceof Activity)
            ActivityCompat.requestPermissions((Activity) context, Utils.PERMS, 8020);
    }

    private static void batchDownloadImpl(@NonNull final Context context, @Nullable final String username, final boolean isFromFeed,
                                          final List<? extends BasePostModel> itemsToDownload) {
        File dir = new File(Environment.getExternalStorageDirectory(), "Download");

        if (sharedPreferences.getBoolean(FOLDER_SAVE_TO, false)) {
            final String customPath = sharedPreferences.getString(FOLDER_PATH, null);
            if (!Utils.isEmpty(customPath)) dir = new File(customPath);
        }

        if (sharedPreferences.getBoolean(Constants.DOWNLOAD_USER_FOLDER, false) && !isEmpty(username))
            dir = new File(dir, username);

        if (dir.exists() || dir.mkdirs()) {
            final Main main = !isFromFeed && context instanceof Main ? (Main) context : null;

            final int itemsToDownloadSize = itemsToDownload.size();

            final File finalDir = dir;
            for (int i = itemsToDownloadSize - 1; i >= 0; i--) {
                final BasePostModel selectedItem = itemsToDownload.get(i);

                if (main == null) {
                    new DownloadAsync(context, selectedItem.getDisplayUrl(),
                            getDownloadSaveFile(finalDir, selectedItem, ""),
                            null).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                } else {
                    new PostFetcher(selectedItem.getShortCode(), result -> {
                        if (result != null) {
                            final int resultsSize = result.length;
                            final boolean multiResult = resultsSize > 1;

                            for (int j = 0; j < resultsSize; j++) {
                                final BasePostModel model = result[j];
                                final File saveFile = getDownloadSaveFile(finalDir, model, multiResult ? "_slide_" + (j + 1) : "");

                                new DownloadAsync(context, model.getDisplayUrl(), saveFile, file -> {
                                    model.setDownloaded(true);
                                    main.mainHelper.deselectSelection(selectedItem);
                                }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            }
                        } else {
                            main.mainHelper.deselectSelection(selectedItem);
                        }
                    }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            }
        } else
            Toast.makeText(context, R.string.error_creating_folders, Toast.LENGTH_SHORT).show();
    }

    @NonNull
    private static File getDownloadSaveFile(final File finalDir, @NonNull final BasePostModel model, final String sliderPrefix) {
        final String displayUrl = model.getDisplayUrl();
        final int index = displayUrl.indexOf('?');
        return new File(finalDir, model.getPostId() + '_' + model.getTimestamp() + sliderPrefix + displayUrl.substring(index - 4, index));
    }

    public static void checkExistence(final File downloadDir, final File customDir, final String username, final boolean isSlider,
                                      final int sliderIndex, @NonNull final BasePostModel model) {
        boolean exists = false;

        try {
            final String displayUrl = model.getDisplayUrl();
            final int index = displayUrl.indexOf('?');

            final String fileName = model.getPostId() + '_' + model.getTimestamp();
            final String extension = displayUrl.substring(index - 4, index);

            final String fileWithoutPrefix = fileName + extension;
            exists = new File(downloadDir, fileWithoutPrefix).exists();
            if (!exists) {
                if (customDir != null) exists = new File(customDir, fileWithoutPrefix).exists();
                if (!exists && !Utils.isEmpty(username)) {
                    exists = new File(new File(downloadDir, username), fileWithoutPrefix).exists();
                }
                if (!exists && customDir != null)
                    exists = new File(new File(customDir, username), fileWithoutPrefix).exists();
            }

            if (!exists && isSlider && sliderIndex != -1) {
                final String fileWithPrefix = fileName + "_slide_[\\d]+" + extension;
                final FilenameFilter filenameFilter = (dir, name) -> Pattern.matches(fileWithPrefix, name);

                File[] files = downloadDir.listFiles(filenameFilter);
                if ((files == null || files.length < 1) && customDir != null)
                    files = customDir.listFiles(filenameFilter);

                if (files != null && files.length >= 1) exists = true;
            }
        } catch (final Exception e) {
            if (BuildConfig.DEBUG) Log.e("AWAISKING_APP", "", e);
        }

        model.setDownloaded(exists);
    }

    public static boolean hasKey(final String key, final String username, final String name) {
        if (!Utils.isEmpty(key)) {
            final boolean hasUserName = username != null && username.toLowerCase().contains(key);
            if (!hasUserName && name != null) return name.toLowerCase().contains(key);
        }
        return true;
    }

    public static void showImportExportDialog(final Context context) {
        final View view = View.inflate(context, R.layout.dialog_import_export, null);

        final CheckBox cbPassword = view.findViewById(R.id.cbPassword);
        final CheckBox cbExportLogins = view.findViewById(R.id.cbExportLogins);
        final CheckBox cbExportSettings = view.findViewById(R.id.cbExportSettings);
        final CheckBox cbExportFavorites = view.findViewById(R.id.cbExportFavorites);

        final CheckBox cbImportLogins = view.findViewById(R.id.cbImportLogins);
        final CheckBox cbImportSettings = view.findViewById(R.id.cbImportSettings);
        final CheckBox cbImportFavorites = view.findViewById(R.id.cbImportFavorites);

        final View passwordParent = (View) cbPassword.getParent();
        final View exportLoginsParent = (View) cbExportLogins.getParent();
        final View exportFavoritesParent = (View) cbExportFavorites.getParent();
        final View exportSettingsParent = (View) cbExportSettings.getParent();
        final View importLoginsParent = (View) cbImportLogins.getParent();
        final View importFavoritesParent = (View) cbImportFavorites.getParent();
        final View importSettingsParent = (View) cbImportSettings.getParent();

        final EditText etPassword = view.findViewById(R.id.etPassword);
        final View btnSaveTo = view.findViewById(R.id.btnSaveTo);
        final View btnImport = view.findViewById(R.id.btnImport);

        cbPassword.setOnCheckedChangeListener((buttonView, isChecked) -> etPassword.setEnabled(isChecked));

        final AlertDialog[] dialog = new AlertDialog[1];
        final View.OnClickListener onClickListener = v -> {
            if (v == passwordParent) cbPassword.performClick();

            else if (v == exportLoginsParent) cbExportLogins.performClick();
            else if (v == exportFavoritesParent) cbExportFavorites.performClick();

            else if (v == importLoginsParent) cbImportLogins.performClick();
            else if (v == importFavoritesParent) cbImportFavorites.performClick();

            else if (v == exportSettingsParent) cbExportSettings.performClick();
            else if (v == importSettingsParent) cbImportSettings.performClick();

            else if (context instanceof AppCompatActivity) {
                final FragmentManager fragmentManager = ((AppCompatActivity) context).getSupportFragmentManager();
                final String folderPath = Utils.sharedPreferences.getString(FOLDER_PATH, null);

                if (v == btnSaveTo) {
                    final Editable text = etPassword.getText();
                    final boolean passwordChecked = cbPassword.isChecked();
                    if (passwordChecked && isEmpty(text))
                        Toast.makeText(context, R.string.dialog_export_err_password_empty, Toast.LENGTH_SHORT).show();
                    else {
                        new DirectoryChooser().setInitialDirectory(folderPath).setInteractionListener(path -> {
                            final File file = new File(path, "InstaGrabber_Settings_" + System.currentTimeMillis() + ".zaai");
                            final String password = passwordChecked ? text.toString() : null;
                            int flags = 0;
                            if (cbExportFavorites.isChecked()) flags |= ExportImportUtils.FLAG_FAVORITES;
                            if (cbExportSettings.isChecked()) flags |= ExportImportUtils.FLAG_SETTINGS;
                            if (cbExportLogins.isChecked()) flags |= ExportImportUtils.FLAG_COOKIES;

                            ExportImportUtils.Export(password, flags, file, result -> {
                                Toast.makeText(context, result ? R.string.dialog_export_success : R.string.dialog_export_failed, Toast.LENGTH_SHORT).show();
                                if (dialog[0] != null && dialog[0].isShowing()) dialog[0].dismiss();
                            });

                        }).show(fragmentManager, null);
                    }

                } else if (v == btnImport) {
                    new DirectoryChooser().setInitialDirectory(folderPath).setShowZaAiConfigFiles(true).setInteractionListener(path -> {
                        int flags = 0;
                        if (cbImportFavorites.isChecked()) flags |= ExportImportUtils.FLAG_FAVORITES;
                        if (cbImportSettings.isChecked()) flags |= ExportImportUtils.FLAG_SETTINGS;
                        if (cbImportLogins.isChecked()) flags |= ExportImportUtils.FLAG_COOKIES;

                        ExportImportUtils.Import(context, flags, new File(path), result -> {
                            ((AppCompatActivity) context).recreate();
                            Toast.makeText(context, result ? R.string.dialog_import_success : R.string.dialog_import_failed, Toast.LENGTH_SHORT).show();
                            if (dialog[0] != null && dialog[0].isShowing()) dialog[0].dismiss();
                        });

                    }).show(fragmentManager, null);
                }
            }
        };

        passwordParent.setOnClickListener(onClickListener);
        exportLoginsParent.setOnClickListener(onClickListener);
        exportSettingsParent.setOnClickListener(onClickListener);
        exportFavoritesParent.setOnClickListener(onClickListener);
        importLoginsParent.setOnClickListener(onClickListener);
        importSettingsParent.setOnClickListener(onClickListener);
        importFavoritesParent.setOnClickListener(onClickListener);
        btnSaveTo.setOnClickListener(onClickListener);
        btnImport.setOnClickListener(onClickListener);

        dialog[0] = new AlertDialog.Builder(context).setView(view).show();
    }

    // taken from Arrays.toString()
    @NonNull
    public static String highlightIdsMerger(final String... strings) {
        if (strings != null) {
            int iMax = strings.length - 1;
            if (iMax != -1) {
                final StringBuilder builder = new StringBuilder();
                builder.append('[');
                for (int i = 0; ; i++) {
                    builder.append('"').append(strings[i]).append('"');
                    if (i == iMax) return builder.append(']').toString();
                    builder.append(',');
                }
            }

        }
        return "[]";
    }

    public static boolean isEmpty(final CharSequence charSequence) {
        if (charSequence == null || charSequence.length() < 1) return true;
        if (charSequence instanceof String) {
            String str = (String) charSequence;
            if ("".equals(str) || "null".equals(str) || str.isEmpty()) return true;
            str = str.trim();
            return "".equals(str) || "null".equals(str) || str.isEmpty();
        }
        return "null".contentEquals(charSequence) || "".contentEquals(charSequence) || charSequence.length() < 1;
    }

    public static boolean isImage(final Uri itemUri, final ContentResolver contentResolver) {
        String mimeType;
        if (itemUri == null) return false;
        final String scheme = itemUri.getScheme();
        if (isEmpty(scheme))
            mimeType = mimeTypeMap.getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(itemUri.toString()).toLowerCase());
        else mimeType = scheme.equals(ContentResolver.SCHEME_CONTENT) ? contentResolver.getType(itemUri)
                : mimeTypeMap.getMimeTypeFromExtension
                (MimeTypeMap.getFileExtensionFromUrl(itemUri.toString()).toLowerCase());

        if (isEmpty(mimeType)) return true;
        mimeType = mimeType.toLowerCase();
        return mimeType.startsWith("image");
    }

    @Nullable
    public static String getCookie(@Nullable final String webViewUrl) {
        int lastLongestCookieLength = 0;
        String mainCookie = null;

        String cookie;
        if (!Utils.isEmpty(webViewUrl)) {
            cookie = Utils.COOKIE_MANAGER.getCookie(webViewUrl);
            if (cookie != null) {
                final int cookieLen = cookie.length();
                if (cookieLen > lastLongestCookieLength) {
                    mainCookie = cookie;
                    lastLongestCookieLength = cookieLen;
                }
            }
        }
        cookie = Utils.COOKIE_MANAGER.getCookie("https://instagram.com");
        if (cookie != null) {
            final int cookieLen = cookie.length();
            if (cookieLen > lastLongestCookieLength) {
                mainCookie = cookie;
                lastLongestCookieLength = cookieLen;
            }
        }
        cookie = Utils.COOKIE_MANAGER.getCookie("https://instagram.com/");
        if (cookie != null) {
            final int cookieLen = cookie.length();
            if (cookieLen > lastLongestCookieLength) {
                mainCookie = cookie;
                lastLongestCookieLength = cookieLen;
            }
        }
        cookie = Utils.COOKIE_MANAGER.getCookie("http://instagram.com");
        if (cookie != null) {
            final int cookieLen = cookie.length();
            if (cookieLen > lastLongestCookieLength) {
                mainCookie = cookie;
                lastLongestCookieLength = cookieLen;
            }
        }
        cookie = Utils.COOKIE_MANAGER.getCookie("http://instagram.com/");
        if (cookie != null) {
            final int cookieLen = cookie.length();
            if (cookieLen > lastLongestCookieLength) {
                mainCookie = cookie;
                lastLongestCookieLength = cookieLen;
            }
        }
        cookie = Utils.COOKIE_MANAGER.getCookie("https://www.instagram.com");
        if (cookie != null) {
            final int cookieLen = cookie.length();
            if (cookieLen > lastLongestCookieLength) {
                mainCookie = cookie;
                lastLongestCookieLength = cookieLen;
            }
        }
        cookie = Utils.COOKIE_MANAGER.getCookie("https://www.instagram.com/");
        if (cookie != null) {
            final int cookieLen = cookie.length();
            if (cookieLen > lastLongestCookieLength) {
                mainCookie = cookie;
                lastLongestCookieLength = cookieLen;
            }
        }
        cookie = Utils.COOKIE_MANAGER.getCookie("http://www.instagram.com");
        if (cookie != null) {
            final int cookieLen = cookie.length();
            if (cookieLen > lastLongestCookieLength) {
                mainCookie = cookie;
                lastLongestCookieLength = cookieLen;
            }
        }
        cookie = Utils.COOKIE_MANAGER.getCookie("http://www.instagram.com/");
        if (cookie != null && cookie.length() > lastLongestCookieLength) mainCookie = cookie;

        return mainCookie;
    }

    public static void errorFinish(@NonNull final Activity activity) {
        Toast.makeText(activity, R.string.downloader_unknown_error, Toast.LENGTH_SHORT).show();
        activity.finish();
    }

    public static boolean isInstaInstalled(@NonNull final Context context) {
        final PackageManager packageManager = context.getPackageManager();
        try {
            packageManager.getPackageInfo("com.instagram.android", 0);
            return true;
        } catch (final Exception e) {
            try {
                return packageManager.getApplicationInfo("com.instagram.android", 0).enabled;
            } catch (final Exception e1) {
                return false;
            }
        }
    }

    @Nullable
    public static String getInstalledTelegramPackage(@NonNull final Context context) {
        final String[] packages = {
                "org.telegram.messenger",
                "org.thunderdog.challegram",
                "ir.ilmili.telegraph",
                "org.telegram.BifToGram",
                "org.vidogram.messenger",
                "com.xplus.messenger",
                "com.ellipi.messenger",
                "org.telegram.plus",
                "com.iMe.android",
                "org.viento.colibri",
                "org.viento.colibrix",
                "ml.parsgram",
                "com.ringtoon.app.tl",
        };

        final PackageManager packageManager = context.getPackageManager();
        for (final String pkg : packages) {
            try {
                final PackageInfo packageInfo = packageManager.getPackageInfo(pkg, 0);
                if (packageInfo.applicationInfo.enabled) return pkg;
            } catch (final Exception e) {
                try {
                    if (packageManager.getApplicationInfo(pkg, 0).enabled) return pkg;
                } catch (final Exception e1) {
                    // meh
                }
            }
        }

        return null;
    }
}