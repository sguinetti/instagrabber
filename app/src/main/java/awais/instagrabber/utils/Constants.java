package awais.instagrabber.utils;

public class Constants {
    // string prefs
    public static final String FOLDER_PATH = "custom_path";
    public static final String DATE_TIME_FORMAT = "date_time_format";
    public static final String DATE_TIME_SELECTION = "date_time_selection";
    // int prefs
    public static final String APP_THEME = "app_theme";
    // boolean prefs
    public static final String DOWNLOAD_USER_FOLDER = "download_user_folder";
    public static final String USE_ALTER_PROFILE = "alternative_profile";
    public static final String BOTTOM_TOOLBAR = "bottom_toolbar";
    public static final String FOLDER_SAVE_TO = "saved_to";
    public static final String AUTOPLAY_VIDEOS = "autoplay_videos";
    public static final String MUTED_VIDEOS = "muted_videos";
    public static final String AUTOLOAD_POSTS = "autoload_posts";
    public static final String SHOW_FEED = "show_feed";
    // never Export
    public static final String COOKIE = "cookie";
    public static final String SHOW_QUICK_ACCESS_DIALOG = "show_quick_dlg";
    //////////////////////// EXTRAS ////////////////////////
    public static final String EXTRAS_USER = "user";
    public static final String EXTRAS_USERNAME = "username";
    public static final String EXTRAS_ID = "id";
    public static final String EXTRAS_POST = "post";
    public static final String EXTRAS_PROFILE = "profile";
    public static final String EXTRAS_TYPE = "type";
    public static final String EXTRAS_NAME = "name";
    public static final String EXTRAS_STORIES = "stories";
    public static final String EXTRAS_HIGHLIGHT = "highlight";
    public static final String EXTRAS_INDEX = "index";
    public static final String EXTRAS_FOLLOWERS = "followers";
    public static final String EXTRAS_SHORTCODE = "shortcode";
}