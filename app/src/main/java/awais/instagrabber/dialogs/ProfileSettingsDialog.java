package awais.instagrabber.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatCheckBox;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import awais.instagrabber.R;
import awais.instagrabber.utils.Utils;

import static awais.instagrabber.utils.Constants.USE_ALTER_PROFILE;

public class ProfileSettingsDialog extends BottomSheetDialogFragment {
    private Activity activity;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);

        final Context context = getContext();
        activity = context instanceof Activity ? (Activity) context : getActivity();

        final View contentView = View.inflate(activity, R.layout.dialog_profile_settings, null);

        final AppCompatCheckBox cbAltProfile = contentView.findViewById(R.id.cbAltProfile);
        cbAltProfile.setChecked(Utils.sharedPreferences.getBoolean(USE_ALTER_PROFILE, false));
        cbAltProfile.setOnCheckedChangeListener((v, checked) ->
                Utils.sharedPreferences.edit().putBoolean(USE_ALTER_PROFILE, checked).apply());
        ((View) cbAltProfile.getParent()).setOnClickListener(v -> cbAltProfile.performClick());

        dialog.setContentView(contentView);

        return dialog;
    }

    @Override
    public void onDismiss(@NonNull final DialogInterface dialog) {
        super.onDismiss(dialog);
        if (activity != null) activity.recreate();
    }
}