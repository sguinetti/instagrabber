package awais.instagrabber.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import awais.instagrabber.R;
import awais.instagrabber.activities.Login;
import awais.instagrabber.utils.DirectoryChooser;
import awais.instagrabber.utils.Utils;
import awaisomereport.CrashReporter;

import static awais.instagrabber.utils.Constants.APP_THEME;
import static awais.instagrabber.utils.Constants.AUTOLOAD_POSTS;
import static awais.instagrabber.utils.Constants.AUTOPLAY_VIDEOS;
import static awais.instagrabber.utils.Constants.BOTTOM_TOOLBAR;
import static awais.instagrabber.utils.Constants.DOWNLOAD_USER_FOLDER;
import static awais.instagrabber.utils.Constants.FOLDER_PATH;
import static awais.instagrabber.utils.Constants.FOLDER_SAVE_TO;
import static awais.instagrabber.utils.Constants.MUTED_VIDEOS;
import static awais.instagrabber.utils.Constants.SHOW_FEED;

public class SettingsDialog extends BottomSheetDialogFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener,
        CompoundButton.OnCheckedChangeListener {
    private Activity activity;
    private FragmentManager fragmentManager;
    private View btnSaveTo, btnImportExport, btnLogin, btnTimeSettings, btnReport;
    private boolean somethingChanged = false;
    private int currentTheme;

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
        if (requestCode != 6200) return;
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) showDirectoryChooser();
        else Toast.makeText(activity, R.string.direct_download_perms_ask, Toast.LENGTH_SHORT).show();
    }

    private void showDirectoryChooser() {
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager == null) fragmentManager = getChildFragmentManager();

        new DirectoryChooser().setInitialDirectory(Utils.sharedPreferences.getString(FOLDER_PATH, null))
                .setInteractionListener(path -> {
                    Utils.sharedPreferences.edit().putString(FOLDER_PATH, path).apply();
                    somethingChanged = true;
                }).show(fragmentManager, null);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable final Bundle savedInstanceState) {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);

        final Context context = getContext();
        activity = context instanceof Activity ? (Activity) context : getActivity();

        fragmentManager = getFragmentManager();
        if (fragmentManager == null) fragmentManager = getChildFragmentManager();

        final View contentView = View.inflate(activity, R.layout.dialog_main_settings, null);

        btnLogin = contentView.findViewById(R.id.btnLogin);
        btnSaveTo = contentView.findViewById(R.id.btnSaveTo);
        btnImportExport = contentView.findViewById(R.id.importExport);
        btnTimeSettings = contentView.findViewById(R.id.btnTimeSettings);
        btnReport = contentView.findViewById(R.id.btnReport);

        Utils.setTooltipText(btnImportExport, R.string.import_export);

        btnLogin.setOnClickListener(this);
        btnReport.setOnClickListener(this);
        btnSaveTo.setOnClickListener(this);
        btnImportExport.setOnClickListener(this);
        btnTimeSettings.setOnClickListener(this);

        final Spinner spAppTheme = contentView.findViewById(R.id.spAppTheme);
        currentTheme = Utils.sharedPreferences.getInt(APP_THEME, 0);
        spAppTheme.setSelection(currentTheme);
        spAppTheme.setOnItemSelectedListener(this);

        final AppCompatCheckBox cbSaveTo = contentView.findViewById(R.id.cbSaveTo);
        final AppCompatCheckBox cbShowFeed = contentView.findViewById(R.id.cbShowFeed);
        final AppCompatCheckBox cbMuteVideos = contentView.findViewById(R.id.cbMuteVideos);
        final AppCompatCheckBox cbBottomToolbar = contentView.findViewById(R.id.cbBottomToolbar);
        final AppCompatCheckBox cbAutoloadPosts = contentView.findViewById(R.id.cbAutoloadPosts);
        final AppCompatCheckBox cbAutoplayVideos = contentView.findViewById(R.id.cbAutoplayVideos);
        final AppCompatCheckBox cbDownloadUsername = contentView.findViewById(R.id.cbDownloadUsername);

        cbSaveTo.setChecked(Utils.sharedPreferences.getBoolean(FOLDER_SAVE_TO, false));
        cbMuteVideos.setChecked(Utils.sharedPreferences.getBoolean(MUTED_VIDEOS, true));
        cbBottomToolbar.setChecked(Utils.sharedPreferences.getBoolean(BOTTOM_TOOLBAR, true));
        cbAutoplayVideos.setChecked(Utils.sharedPreferences.getBoolean(AUTOPLAY_VIDEOS, true));

        cbShowFeed.setChecked(Utils.sharedPreferences.getBoolean(SHOW_FEED, false));
        cbAutoloadPosts.setChecked(Utils.sharedPreferences.getBoolean(AUTOLOAD_POSTS, false));
        cbDownloadUsername.setChecked(Utils.sharedPreferences.getBoolean(DOWNLOAD_USER_FOLDER, false));

        setupListener(cbSaveTo);
        setupListener(cbShowFeed);
        setupListener(cbMuteVideos);
        setupListener(cbBottomToolbar);
        setupListener(cbAutoloadPosts);
        setupListener(cbAutoplayVideos);
        setupListener(cbDownloadUsername);

        btnSaveTo.setEnabled(cbSaveTo.isChecked());

        dialog.setContentView(contentView);

        return dialog;
    }

    private void setupListener(@NonNull final AppCompatCheckBox checkBox) {
        checkBox.setOnCheckedChangeListener(this);
        ((View) checkBox.getParent()).setOnClickListener(this);
    }

    @Override
    public void onItemSelected(final AdapterView<?> parent, final View view, final int position, final long id) {
        if (position != currentTheme) {
            Utils.sharedPreferences.edit().putInt(APP_THEME, position).apply();
            somethingChanged = true;
        }
    }

    @Override
    public void onClick(final View v) {
        if (v == btnLogin) {
            startActivity(new Intent(v.getContext(), Login.class));
            somethingChanged = true;

        } else if (v == btnImportExport) {
            Utils.showImportExportDialog(activity);

        } else if (v == btnTimeSettings) {
            new TimeSettingsDialog().show(fragmentManager, null);

        } else if (v == btnReport) {
            CrashReporter.get(activity.getApplication()).zipLogs().startCrashEmailIntent(activity, true);

        } else if (v == btnSaveTo) {
            if (ContextCompat.checkSelfPermission(activity, Utils.PERMS[0]) == PackageManager.PERMISSION_DENIED)
                requestPermissions(Utils.PERMS, 6200);
            else showDirectoryChooser();

        } else if (v instanceof ViewGroup)
            ((ViewGroup) v).getChildAt(0).performClick();
    }

    @Override
    public void onCheckedChanged(@NonNull final CompoundButton checkBox, final boolean checked) {
        final SharedPreferences.Editor editor = Utils.sharedPreferences.edit();
        final int id = checkBox.getId();
        if (id == R.id.cbDownloadUsername) editor.putBoolean(DOWNLOAD_USER_FOLDER, checked);
        else if (id == R.id.cbBottomToolbar) editor.putBoolean(BOTTOM_TOOLBAR, checked);
        else if (id == R.id.cbAutoplayVideos) editor.putBoolean(AUTOPLAY_VIDEOS, checked);
        else if (id == R.id.cbMuteVideos) editor.putBoolean(MUTED_VIDEOS, checked);
        else if (id == R.id.cbAutoloadPosts) editor.putBoolean(AUTOLOAD_POSTS, checked);
        else if (id == R.id.cbShowFeed) editor.putBoolean(SHOW_FEED, checked);
        else if (id == R.id.cbSaveTo) {
            editor.putBoolean(FOLDER_SAVE_TO, checked);
            btnSaveTo.setEnabled(checked);
        }
        editor.apply();
        somethingChanged = true;
    }

    @Override
    public void onDismiss(@NonNull final DialogInterface dialog) {
        super.onDismiss(dialog);
        if (somethingChanged && activity != null) activity.recreate();
    }

    @Override
    public void onNothingSelected(final AdapterView<?> parent) { }
}