package awais.instagrabber.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import awais.instagrabber.R;
import awais.instagrabber.utils.Constants;
import awais.instagrabber.utils.Utils;

import static awais.instagrabber.utils.Utils.sharedPreferences;

public class TimeSettingsDialog extends DialogFragment implements AdapterView.OnItemSelectedListener, CompoundButton.OnCheckedChangeListener, View.OnClickListener {
    private final Date magicDate;
    private SimpleDateFormat currentFormat;
    private String selectedFormat;
    private TextView timePreview;
    private Spinner spTimeFormat;
    private Spinner spDateFormat;
    private Spinner spSeparator;
    private CheckBox cbSwapTimeDate;

    public TimeSettingsDialog() {
        super();
        final Calendar instance = GregorianCalendar.getInstance();
        instance.set(2020, 5, 22, 8, 17, 13);
        magicDate = instance.getTime();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable final Bundle savedInstanceState) {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
        final View contentView = View.inflate(getContext(), R.layout.dialog_time_settings, null);

        timePreview = contentView.findViewById(R.id.timePreview);

        spTimeFormat = contentView.findViewById(R.id.spTimeFormat);
        spDateFormat = contentView.findViewById(R.id.spDateFormat);
        spSeparator = contentView.findViewById(R.id.spSeparator);

        final String[] dateTimeFormat = sharedPreferences.getString(Constants.DATE_TIME_SELECTION, "0;3;0")
                .split(";"); // output = time;separator;date
        spTimeFormat.setSelection(Integer.parseInt(dateTimeFormat[0]));
        spSeparator.setSelection(Integer.parseInt(dateTimeFormat[1]));
        spDateFormat.setSelection(Integer.parseInt(dateTimeFormat[2]));

        (cbSwapTimeDate = contentView.findViewById(R.id.cbSwapTimeDate)).setOnCheckedChangeListener(this);
        contentView.findViewById(R.id.btnConfirm).setOnClickListener(this);

        refreshTimeFormat();

        spTimeFormat.setOnItemSelectedListener(this);
        spDateFormat.setOnItemSelectedListener(this);
        spSeparator.setOnItemSelectedListener(this);

        // Utils.datetimeParser

        dialog.setContentView(contentView);
        return dialog;
    }

    private void refreshTimeFormat() {
        final String sepStr = String.valueOf(spSeparator.getSelectedItem());
        final String timeStr = String.valueOf(spTimeFormat.getSelectedItem());
        final String dateStr = String.valueOf(spDateFormat.getSelectedItem());

        final boolean isSwapTime = !cbSwapTimeDate.isChecked();

        selectedFormat = (isSwapTime ? timeStr : dateStr)
                + (Utils.isEmpty(sepStr) || spSeparator.getSelectedItemPosition() == 0 ? " " : " '" + sepStr + "' ")
                + (isSwapTime ? dateStr : timeStr);

        timePreview.setText((currentFormat = new SimpleDateFormat(selectedFormat, Locale.getDefault())).format(magicDate));
    }

    @Override
    public void onItemSelected(final AdapterView<?> p, final View v, final int pos, final long id) {
        refreshTimeFormat();
    }

    @Override
    public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
        refreshTimeFormat();
    }

    @Override
    public void onClick(final View v) {// time;separator;date
        final String formatSelection = spTimeFormat.getSelectedItemPosition() + ";" + spSeparator.getSelectedItemPosition()
                + ';' + spDateFormat.getSelectedItemPosition();

        Utils.sharedPreferences.edit()
                .putString(Constants.DATE_TIME_FORMAT, selectedFormat)
                .putString(Constants.DATE_TIME_SELECTION, formatSelection)
                .apply();

        Utils.datetimeParser = (SimpleDateFormat) currentFormat.clone();
        dismiss();
    }

    @Override
    public void onNothingSelected(final AdapterView<?> parent) { }
}