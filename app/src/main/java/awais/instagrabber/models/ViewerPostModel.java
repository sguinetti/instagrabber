package awais.instagrabber.models;

public final class ViewerPostModel extends BasePostModel {
    protected final String username;
    protected final long videoViews;
    protected String sliderDisplayUrl;
    protected long commentsCount;

    public ViewerPostModel(final boolean isVideo, final String postId, final String displayUrl, final String shortCode,
                           final String postCaption, final String username, final long videoViews, final long timestamp) {
        this.isVideo = isVideo;
        this.postId = postId;
        this.displayUrl = displayUrl;
        this.postCaption = postCaption;
        this.username = username;
        this.shortCode = shortCode;
        this.videoViews = videoViews;
        this.timestamp = timestamp;
    }

    public long getCommentsCount() {
        return commentsCount;
    }

    public String getSliderDisplayUrl() {
        return sliderDisplayUrl;
    }

    public String getUsername() {
        return username;
    }

    public final long getVideoViews() {
        return videoViews;
    }

    public void setSliderDisplayUrl(final String sliderDisplayUrl) {
        this.sliderDisplayUrl = sliderDisplayUrl;
    }

    public void setCommentsCount(final long commentsCount) {
        this.commentsCount = commentsCount;
    }
}