package awais.instagrabber.models;

import java.io.Serializable;

public final class StoryModel implements Serializable {
    private final String storyMediaId, storyUrl;
    private final boolean isVideo;
    private final long timestamp;
    private String videoUrl, tappableShortCode;
    private int position;

    public StoryModel(final String storyMediaId, final String storyUrl, final boolean isVideo, final long timestamp) {
        this.storyMediaId = storyMediaId;
        this.storyUrl = storyUrl;
        this.isVideo = isVideo;
        this.timestamp = timestamp;
    }

    public String getStoryUrl() {
        return storyUrl;
    }

    public String getStoryMediaId() {
        return storyMediaId;
    }

    public boolean isVideo() {
        return isVideo;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public String getTappableShortCode() {
        return tappableShortCode;
    }

    public int getPosition() {
        return position;
    }

    public void setVideoUrl(final String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public void setTappableShortCode(final String tappableShortCode) {
        this.tappableShortCode = tappableShortCode;
    }

    public void setPosition(final int position) {
        this.position = position;
    }
}