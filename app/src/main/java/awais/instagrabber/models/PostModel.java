package awais.instagrabber.models;

public class PostModel extends BasePostModel {
    protected final String thumbnailUrl;
    protected String endCursor;
    protected boolean hasNextPage, isSlider;

    public PostModel(final String shortCode) {
        this.shortCode = shortCode;
        this.thumbnailUrl = null;
    }

    public PostModel(final boolean isVideo, final String postId, final String displayUrl, final String thumbnailUrl,
                     final String shortCode, final CharSequence postCaption, long timestamp) {
        this.isVideo = isVideo;
        this.postId = postId;
        this.displayUrl = displayUrl;
        this.thumbnailUrl = thumbnailUrl;
        this.shortCode = shortCode;
        this.postCaption = postCaption;
        this.timestamp = timestamp;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public String getEndCursor() {
        return endCursor;
    }

    public boolean isSlider() {
        return isSlider;
    }

    public boolean hasNextPage() {
        return endCursor != null && hasNextPage;
    }

    public void setPostCaption(final CharSequence postCaption) {
        this.postCaption = postCaption;
    }

    public void setTimestamp(final long timestamp) {
        this.timestamp = timestamp;
    }

    public void setSlider(final boolean slider) {
        isSlider = slider;
    }

    public void setPageCursor(final boolean hasNextPage, final String endCursor) {
        this.endCursor = endCursor;
        this.hasNextPage = hasNextPage;
    }
}