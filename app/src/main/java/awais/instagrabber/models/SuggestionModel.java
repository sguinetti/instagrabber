package awais.instagrabber.models;

import awais.instagrabber.asyncs.SuggestionsFetcher;

public final class SuggestionModel {
    private final boolean isVerified;
    private final String username, name, profilePic;
    private final SuggestionsFetcher.SuggestionType suggestionType;

    public SuggestionModel(final boolean isVerified, final String username, final String name, final String profilePic,
                           final SuggestionsFetcher.SuggestionType suggestionType) {
        this.isVerified = isVerified;
        this.username = username;
        this.name = name;
        this.profilePic = profilePic;
        this.suggestionType = suggestionType;
    }

    public boolean isVerified() {
        return isVerified;
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public SuggestionsFetcher.SuggestionType getSuggestionType() {
        return suggestionType;
    }
}
