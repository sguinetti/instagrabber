package awais.instagrabber.utils;

import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.RelativeSizeSpan;
import android.text.style.URLSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import awais.instagrabber.BuildConfig;
import awais.instagrabber.R;
import awais.instagrabber.interfaces.FetchListener;

import static awais.instagrabber.utils.Utils.sharedPreferences;

public class FlavorTown {
    public static void updateCheck(@NonNull final Context context) {
        new UpdateChecker(result -> Toast.makeText(context,
                R.string.update_available, Toast.LENGTH_SHORT).show()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public static void changelogCheck(@NonNull final Context context) {
        if (sharedPreferences.getInt("prevVersion", -1) < BuildConfig.VERSION_CODE) {
            new ChangelogFetcher(new FetchListener<CharSequence>() {
                private AlertDialog alertDialog;
                private TextView textView;

                @Override
                public void doBefore() {
                    final ViewGroup rootView = (ViewGroup) View.inflate(context, R.layout.layout_changelog_textview, null);
                    textView = (TextView) rootView.getChildAt(0);
                    textView.setMovementMethod(new LinkMovementMethod());
                    alertDialog = new AlertDialog.Builder(context).setTitle(R.string.title_changelog).setView(rootView).show();
                }

                @Override
                public void onResult(final CharSequence result) {
                    if (alertDialog != null && alertDialog.isShowing() && textView != null && !Utils.isEmpty(result)) {
                        final SpannableStringBuilder stringBuilder = new SpannableStringBuilder();

                        final Resources resources = context.getResources();

                        final String version = BuildConfig.VERSION_NAME.substring(0, BuildConfig.VERSION_NAME.indexOf('-'));
                        stringBuilder.append(resources.getString(R.string.curr_version, version)).append('\n');

                        stringBuilder.setSpan(new RelativeSizeSpan(1.3f), 0, stringBuilder.length() - 1, 0);

                        final int resLen = result.length();
                        int versionTimes = 0;

                        for (int i = 0; i < resLen; ++i) {
                            final char c = result.charAt(i);

                            if (c == 'v' && i > 0) {
                                final char c1 = result.charAt(i - 1);
                                if (c1 == '\r' || c1 == '\n') {
                                    if (++versionTimes == 4) break;
                                }
                            }

                            stringBuilder.append(c);
                        }

                        final String strReadMore = resources.getString(R.string.read_more);
                        stringBuilder.append('\n').append(strReadMore);

                        final int sbLen = stringBuilder.length();
                        stringBuilder.setSpan(new URLSpan("https://gitlab.com/AwaisKing/instagrabber/-/blob/master/CHANGELOG"),
                                sbLen - strReadMore.length(), sbLen, 0);

                        textView.setText(stringBuilder, TextView.BufferType.SPANNABLE);
                    }

                    sharedPreferences.edit().putInt("prevVersion", BuildConfig.VERSION_CODE).apply();
                }
            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }
}