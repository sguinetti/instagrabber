NOTA: Asegúrate de descargar la última versión en la página de Releases en GitLab
https://gitlab.com/AwaisKing/instagrabber/-/releases

Una app simple de descargas para Instagram (+soporte de acceso).

Lo que puedes hacer:

   * buscar nombres de usuario y hashtags

   * comprobar y comparar seguidores/siguiendo

   * ¡ver las noticias y las publicaciones más importantes! (sólo con acceso)

   * descargar fotos de perfil y sus publicaciones.

   * ¡Descargue historias y descatacados! (sólo con acceso)

   * descargar publicaciones en lote.

   * y más!
     (revisa https://gitlab.com/AwaisKing/instagrabber/blob/master/README.md)
