+ added discover/explore page (only for logged in users)
+ added function to remove IPTC tracking data from downloaded pictures (thanks to Airikr [@edgren] on Telegram)
+ added multiple accounts support (quick access) and favorites (a suggestion from Saurabh on Telegram)
+ added custom download folder option, you can select where to download posts (a suggestion from Airikr)
+ added desktop mode toggle in Login activity (a suggestion from Eymen on Telegram)
+ added post date in post viewer (a suggestion from W on Telegram)
+ added post time format settings [ Settings > Post Time Settings ]
+ fixed some icons and layouts
+ removed color from slider items in feed
+ removed useless methods and properties
+ better way of handling multiple stories and posts (sliders) in post viewer
+ tried to make notifications grouped together.. hope they work
+ some other fixes and additions which i probably forgot, cause i'm a human ffs
