NOTE: Make sure to check the latest version on Releases page on GitLab
https://gitlab.com/AwaisKing/instagrabber/-/releases

A simple downloader app for Instagram (+login support).

What you can do:

   * search for usernames and hashtags

   * check and compare followers/following

   * view your feed and top posts! (logged in only)

   * download profile pictures and their posts.

   * download stories and highlights!! (logged in only)

   * download batch items.

   * and more!
     (read https://gitlab.com/AwaisKing/instagrabber/blob/master/README.md)
